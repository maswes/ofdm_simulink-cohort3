-- -------------------------------------------------------------
-- 
-- File Name: C:\temp\simulink_code_gen\hdl_prj\hdlsrc\phy_layer53\dc_offset.vhd
-- Created: 2015-05-30 21:50:19
-- 
-- Generated by MATLAB 8.3 and HDL Coder 3.4
-- 
-- 
-- -------------------------------------------------------------
-- Rate and Clocking Details
-- -------------------------------------------------------------
-- Model base rate: 0.5
-- Target subsystem base rate: 1
-- 
-- 
-- Clock Enable  Sample Time
-- -------------------------------------------------------------
-- ce_out        1
-- -------------------------------------------------------------
-- 
-- 
-- Output Signal                 Clock Enable  Sample Time
-- -------------------------------------------------------------
-- dc_i_out                      ce_out        1
-- dc_q_out                      ce_out        1
-- dc_rssi_out                   ce_out        1
-- dc_gain_out                   ce_out        1
-- dc_i_mean_out                 ce_out        1
-- dc_i_dc_out                   ce_out        1
-- dc_dir_out                    ce_out        1
-- dc_blinky                     ce_out        1
-- -------------------------------------------------------------
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: dc_offset
-- Source Path: phy_layer53/dc_offset
-- Hierarchy Level: 0
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY dc_offset IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        clk_enable                        :   IN    std_logic;
        dc_dig_i_in                       :   IN    std_logic_vector(11 DOWNTO 0);  -- sfix12
        dc_dig_q_in                       :   IN    std_logic_vector(11 DOWNTO 0);  -- sfix12
        dc_rf_i_in                        :   IN    std_logic_vector(11 DOWNTO 0);  -- sfix12
        dc_rf_q_in                        :   IN    std_logic_vector(11 DOWNTO 0);  -- sfix12
        dig_looback_en                    :   IN    std_logic;  -- ufix1
        freq_acq_en                       :   IN    std_logic;  -- ufix1
        ce_out                            :   OUT   std_logic;
        dc_i_out                          :   OUT   std_logic_vector(11 DOWNTO 0);  -- sfix12
        dc_q_out                          :   OUT   std_logic_vector(11 DOWNTO 0);  -- sfix12
        dc_rssi_out                       :   OUT   std_logic_vector(23 DOWNTO 0);  -- ufix24
        dc_gain_out                       :   OUT   std_logic_vector(5 DOWNTO 0);  -- ufix6
        dc_i_mean_out                     :   OUT   std_logic_vector(23 DOWNTO 0);  -- sfix24_En12
        dc_i_dc_out                       :   OUT   std_logic_vector(23 DOWNTO 0);  -- sfix24_En12
        dc_dir_out                        :   OUT   std_logic_vector(3 DOWNTO 0);  -- ufix4
        dc_blinky                         :   OUT   std_logic  -- ufix1
        );
END dc_offset;


ARCHITECTURE rtl OF dc_offset IS

  -- Component Declarations
  COMPONENT MATLAB_Function
    PORT( clk                             :   IN    std_logic;
          reset                           :   IN    std_logic;
          enb_1_2_0                       :   IN    std_logic;
          enb_const_rate                  :   IN    std_logic;
          i_in                            :   IN    std_logic_vector(25 DOWNTO 0);  -- sfix26_En12
          q_in                            :   IN    std_logic_vector(25 DOWNTO 0);  -- sfix26_En12
          gain_en_in                      :   IN    std_logic;  -- ufix1
          rssi_low_goal_in                :   IN    std_logic_vector(29 DOWNTO 0);  -- ufix30
          rssi_high_goal_in               :   IN    std_logic_vector(29 DOWNTO 0);  -- ufix30
          rx_en_in                        :   IN    std_logic;  -- ufix1
          gain_in                         :   IN    std_logic_vector(5 DOWNTO 0);  -- ufix6
          i_out                           :   OUT   std_logic_vector(26 DOWNTO 0);  -- sfix27_En12
          q_out                           :   OUT   std_logic_vector(26 DOWNTO 0);  -- sfix27_En12
          rssi_out                        :   OUT   std_logic_vector(23 DOWNTO 0);  -- ufix24
          dir_out                         :   OUT   std_logic_vector(3 DOWNTO 0);  -- ufix4
          blinky                          :   OUT   std_logic;  -- ufix1
          d1                              :   OUT   std_logic_vector(23 DOWNTO 0);  -- ufix24
          d2                              :   OUT   std_logic_vector(54 DOWNTO 0);  -- sfix55_En24
          d3                              :   OUT   std_logic_vector(23 DOWNTO 0);  -- ufix24
          d4                              :   OUT   std_logic_vector(23 DOWNTO 0);  -- ufix24
          d5                              :   OUT   std_logic_vector(3 DOWNTO 0);  -- ufix4
          d6                              :   OUT   std_logic_vector(27 DOWNTO 0);  -- ufix28
          d7                              :   OUT   std_logic;  -- ufix1
          d8                              :   OUT   std_logic;  -- ufix1
          gain_out                        :   OUT   std_logic_vector(5 DOWNTO 0);  -- ufix6
          i_mean_out                      :   OUT   std_logic_vector(23 DOWNTO 0);  -- sfix24_En12
          i_dc_out                        :   OUT   std_logic_vector(23 DOWNTO 0)  -- sfix24_En12
          );
  END COMPONENT;

  -- Component Configuration Statements
  FOR ALL : MATLAB_Function
    USE ENTITY work.MATLAB_Function(rtl);

  -- Signals
  SIGNAL enb_1_2_0                        : std_logic;
  SIGNAL enb_const_rate                   : std_logic;
  SIGNAL dig_looback_en_1                 : std_logic;  -- ufix1
  SIGNAL Delay10_out1                     : std_logic;  -- ufix1
  SIGNAL Delay10_out1_1                   : std_logic;  -- ufix1
  SIGNAL switch_compare_1                 : std_logic;
  SIGNAL dc_rf_i_in_signed                : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL dc_rf_i_in_1                     : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL Delay13_out1                     : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL dc_dig_i_in_signed               : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL dc_dig_i_in_1                    : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL Gain_out1                        : signed(23 DOWNTO 0);  -- sfix24_En14
  SIGNAL Delay11_out1                     : signed(23 DOWNTO 0);  -- sfix24_En14
  SIGNAL switch_compare_1_1               : std_logic;
  SIGNAL dc_rf_q_in_signed                : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL dc_rf_q_in_1                     : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL Delay14_out1                     : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL dc_dig_q_in_signed               : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL dc_dig_q_in_1                    : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL Gain1_out1                       : signed(23 DOWNTO 0);  -- sfix24_En14
  SIGNAL Delay12_out1                     : signed(23 DOWNTO 0);  -- sfix24_En14
  SIGNAL Delay13_out1_1                   : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL Delay11_out1_1                   : signed(23 DOWNTO 0);  -- sfix24_En14
  SIGNAL Delay11_out1_dtc                 : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL i_in                             : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL i_in_1                           : signed(25 DOWNTO 0);  -- sfix26_En12
  SIGNAL Delay14_out1_1                   : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL Delay12_out1_1                   : signed(23 DOWNTO 0);  -- sfix24_En14
  SIGNAL Delay12_out1_dtc                 : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL q_in                             : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL q_in_1                           : signed(25 DOWNTO 0);  -- sfix26_En12
  SIGNAL Constant9_out1                   : std_logic;  -- ufix1
  SIGNAL Constant8_out1                   : unsigned(29 DOWNTO 0);  -- ufix30
  SIGNAL Constant7_out1                   : unsigned(29 DOWNTO 0);  -- ufix30
  SIGNAL freq_acq_en_1                    : std_logic;  -- ufix1
  SIGNAL Delay4_out1                      : std_logic;  -- ufix1
  SIGNAL Delay4_out1_1                    : std_logic;  -- ufix1
  SIGNAL Constant_out1                    : unsigned(5 DOWNTO 0);  -- ufix6
  SIGNAL i_out                            : std_logic_vector(26 DOWNTO 0);  -- ufix27
  SIGNAL q_out                            : std_logic_vector(26 DOWNTO 0);  -- ufix27
  SIGNAL rssi_out                         : std_logic_vector(23 DOWNTO 0);  -- ufix24
  SIGNAL dir_out                          : std_logic_vector(3 DOWNTO 0);  -- ufix4
  SIGNAL blinky                           : std_logic;  -- ufix1
  SIGNAL d1                               : std_logic_vector(23 DOWNTO 0);  -- ufix24
  SIGNAL d2                               : std_logic_vector(54 DOWNTO 0);  -- ufix55
  SIGNAL d3                               : std_logic_vector(23 DOWNTO 0);  -- ufix24
  SIGNAL d4                               : std_logic_vector(23 DOWNTO 0);  -- ufix24
  SIGNAL d5                               : std_logic_vector(3 DOWNTO 0);  -- ufix4
  SIGNAL d6                               : std_logic_vector(27 DOWNTO 0);  -- ufix28
  SIGNAL d7                               : std_logic;  -- ufix1
  SIGNAL d8                               : std_logic;  -- ufix1
  SIGNAL gain_out                         : std_logic_vector(5 DOWNTO 0);  -- ufix6
  SIGNAL i_mean_out                       : std_logic_vector(23 DOWNTO 0);  -- ufix24
  SIGNAL i_dc_out                         : std_logic_vector(23 DOWNTO 0);  -- ufix24
  SIGNAL i_out_signed                     : signed(26 DOWNTO 0);  -- sfix27_En12
  SIGNAL Data_Type_Conversion2_out1       : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL i_in_2                           : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL i_in_3                           : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL i_in_4                           : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL q_out_signed                     : signed(26 DOWNTO 0);  -- sfix27_En12
  SIGNAL Data_Type_Conversion3_out1       : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL q_in_2                           : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL q_in_3                           : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL q_in_4                           : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL rssi_out_unsigned                : unsigned(23 DOWNTO 0);  -- ufix24
  SIGNAL rssi_out_1                       : unsigned(23 DOWNTO 0);  -- ufix24
  SIGNAL rssi_out_2                       : unsigned(23 DOWNTO 0);  -- ufix24
  SIGNAL rssi_out_3                       : unsigned(23 DOWNTO 0);  -- ufix24
  SIGNAL gain_out_unsigned                : unsigned(5 DOWNTO 0);  -- ufix6
  SIGNAL gain_out_1                       : unsigned(5 DOWNTO 0);  -- ufix6
  SIGNAL gain_out_2                       : unsigned(5 DOWNTO 0);  -- ufix6
  SIGNAL gain_out_3                       : unsigned(5 DOWNTO 0);  -- ufix6
  SIGNAL i_mean_out_signed                : signed(23 DOWNTO 0);  -- sfix24_En12
  SIGNAL i_mean_out_1                     : signed(23 DOWNTO 0);  -- sfix24_En12
  SIGNAL i_mean_out_2                     : signed(23 DOWNTO 0);  -- sfix24_En12
  SIGNAL i_mean_out_3                     : signed(23 DOWNTO 0);  -- sfix24_En12
  SIGNAL i_dc_out_signed                  : signed(23 DOWNTO 0);  -- sfix24_En12
  SIGNAL i_dc_out_1                       : signed(23 DOWNTO 0);  -- sfix24_En12
  SIGNAL i_dc_out_2                       : signed(23 DOWNTO 0);  -- sfix24_En12
  SIGNAL i_dc_out_3                       : signed(23 DOWNTO 0);  -- sfix24_En12
  SIGNAL dir_out_unsigned                 : unsigned(3 DOWNTO 0);  -- ufix4
  SIGNAL dir_out_1                        : unsigned(3 DOWNTO 0);  -- ufix4
  SIGNAL dir_out_2                        : unsigned(3 DOWNTO 0);  -- ufix4
  SIGNAL dir_out_3                        : unsigned(3 DOWNTO 0);  -- ufix4
  SIGNAL blinky_1                         : std_logic;  -- ufix1
  SIGNAL blinky_2                         : std_logic;  -- ufix1
  SIGNAL blinky_3                         : std_logic;  -- ufix1

BEGIN
  -- dc_dig_q_in

  -- <S10>/MATLAB Function
  u_MATLAB_Function : MATLAB_Function
    PORT MAP( clk => clk,
              reset => reset,
              enb_1_2_0 => clk_enable,
              enb_const_rate => clk_enable,
              i_in => std_logic_vector(i_in_1),  -- sfix26_En12
              q_in => std_logic_vector(q_in_1),  -- sfix26_En12
              gain_en_in => Constant9_out1,  -- ufix1
              rssi_low_goal_in => std_logic_vector(Constant8_out1),  -- ufix30
              rssi_high_goal_in => std_logic_vector(Constant7_out1),  -- ufix30
              rx_en_in => Delay4_out1_1,  -- ufix1
              gain_in => std_logic_vector(Constant_out1),  -- ufix6
              i_out => i_out,  -- sfix27_En12
              q_out => q_out,  -- sfix27_En12
              rssi_out => rssi_out,  -- ufix24
              dir_out => dir_out,  -- ufix4
              blinky => blinky,  -- ufix1
              d1 => d1,  -- ufix24
              d2 => d2,  -- sfix55_En24
              d3 => d3,  -- ufix24
              d4 => d4,  -- ufix24
              d5 => d5,  -- ufix4
              d6 => d6,  -- ufix28
              d7 => d7,  -- ufix1
              d8 => d8,  -- ufix1
              gain_out => gain_out,  -- ufix6
              i_mean_out => i_mean_out,  -- sfix24_En12
              i_dc_out => i_dc_out  -- sfix24_En12
              );

  enb_const_rate <= clk_enable;

  in_4_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      dig_looback_en_1 <= '0';
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_const_rate = '1' THEN
        dig_looback_en_1 <= dig_looback_en;
      END IF;
    END IF;
  END PROCESS in_4_pipe_process;


  enb_1_2_0 <= clk_enable;

  -- <S10>/Delay10
  Delay10_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      Delay10_out1 <= '0';
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        Delay10_out1 <= dig_looback_en_1;
      END IF;
    END IF;
  END PROCESS Delay10_process;


  Delay10_out_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      Delay10_out1_1 <= '0';
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        Delay10_out1_1 <= Delay10_out1;
      END IF;
    END IF;
  END PROCESS Delay10_out_pipe_process;


  
  switch_compare_1 <= '1' WHEN Delay10_out1_1 > '0' ELSE
      '0';

  dc_rf_i_in_signed <= signed(dc_rf_i_in);

  in_2_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      dc_rf_i_in_1 <= to_signed(2#000000000000#, 12);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_const_rate = '1' THEN
        dc_rf_i_in_1 <= dc_rf_i_in_signed;
      END IF;
    END IF;
  END PROCESS in_2_pipe_process;


  -- <S10>/Delay13
  Delay13_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      Delay13_out1 <= to_signed(2#000000000000#, 12);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        Delay13_out1 <= dc_rf_i_in_1;
      END IF;
    END IF;
  END PROCESS Delay13_process;


  dc_dig_i_in_signed <= signed(dc_dig_i_in);

  in_0_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      dc_dig_i_in_1 <= to_signed(2#000000000000#, 12);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        dc_dig_i_in_1 <= dc_dig_i_in_signed;
      END IF;
    END IF;
  END PROCESS in_0_pipe_process;


  -- <S10>/Gain
  Gain_out1 <= 1638 * dc_dig_i_in_1;

  -- <S10>/Delay11
  Delay11_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      Delay11_out1 <= to_signed(16#000000#, 24);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        Delay11_out1 <= Gain_out1;
      END IF;
    END IF;
  END PROCESS Delay11_process;


  
  switch_compare_1_1 <= '1' WHEN Delay10_out1_1 > '0' ELSE
      '0';

  dc_rf_q_in_signed <= signed(dc_rf_q_in);

  in_3_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      dc_rf_q_in_1 <= to_signed(2#000000000000#, 12);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_const_rate = '1' THEN
        dc_rf_q_in_1 <= dc_rf_q_in_signed;
      END IF;
    END IF;
  END PROCESS in_3_pipe_process;


  -- <S10>/Delay14
  Delay14_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      Delay14_out1 <= to_signed(2#000000000000#, 12);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        Delay14_out1 <= dc_rf_q_in_1;
      END IF;
    END IF;
  END PROCESS Delay14_process;


  dc_dig_q_in_signed <= signed(dc_dig_q_in);

  in_1_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      dc_dig_q_in_1 <= to_signed(2#000000000000#, 12);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        dc_dig_q_in_1 <= dc_dig_q_in_signed;
      END IF;
    END IF;
  END PROCESS in_1_pipe_process;


  -- <S10>/Gain1
  Gain1_out1 <= 1638 * dc_dig_q_in_1;

  -- <S10>/Delay12
  Delay12_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      Delay12_out1 <= to_signed(16#000000#, 24);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        Delay12_out1 <= Gain1_out1;
      END IF;
    END IF;
  END PROCESS Delay12_process;


  Delay13_out_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      Delay13_out1_1 <= to_signed(2#000000000000#, 12);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        Delay13_out1_1 <= Delay13_out1;
      END IF;
    END IF;
  END PROCESS Delay13_out_pipe_process;


  Delay11_out_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      Delay11_out1_1 <= to_signed(16#000000#, 24);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        Delay11_out1_1 <= Delay11_out1;
      END IF;
    END IF;
  END PROCESS Delay11_out_pipe_process;


  Delay11_out1_dtc <= resize(Delay11_out1_1(23 DOWNTO 14), 12);

  -- <S10>/Switch1
  
  i_in <= Delay13_out1_1 WHEN switch_compare_1 = '0' ELSE
      Delay11_out1_dtc;

  -- <S10>/Data Type Conversion4
  -- 
  -- <S10>/Shift Arithmetic
  i_in_1 <= resize(i_in & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0', 26);

  Delay14_out_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      Delay14_out1_1 <= to_signed(2#000000000000#, 12);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        Delay14_out1_1 <= Delay14_out1;
      END IF;
    END IF;
  END PROCESS Delay14_out_pipe_process;


  Delay12_out_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      Delay12_out1_1 <= to_signed(16#000000#, 24);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        Delay12_out1_1 <= Delay12_out1;
      END IF;
    END IF;
  END PROCESS Delay12_out_pipe_process;


  Delay12_out1_dtc <= resize(Delay12_out1_1(23 DOWNTO 14), 12);

  -- <S10>/Switch
  
  q_in <= Delay14_out1_1 WHEN switch_compare_1_1 = '0' ELSE
      Delay12_out1_dtc;

  -- <S10>/Data Type Conversion5
  q_in_1 <= resize(q_in & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0', 26);

  -- <S10>/Constant9
  Constant9_out1 <= '1';

  -- <S10>/Constant8
  Constant8_out1 <= to_unsigned(2#000000000100011111001000111000#, 30);

  -- <S10>/Constant7
  Constant7_out1 <= to_unsigned(2#000000000100101010110110111000#, 30);

  in_5_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      freq_acq_en_1 <= '0';
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        freq_acq_en_1 <= freq_acq_en;
      END IF;
    END IF;
  END PROCESS in_5_pipe_process;


  -- <S10>/Delay4
  Delay4_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      Delay4_out1 <= '0';
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        Delay4_out1 <= freq_acq_en_1;
      END IF;
    END IF;
  END PROCESS Delay4_process;


  Delay4_out_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      Delay4_out1_1 <= '0';
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        Delay4_out1_1 <= Delay4_out1;
      END IF;
    END IF;
  END PROCESS Delay4_out_pipe_process;


  -- <S10>/Constant
  Constant_out1 <= to_unsigned(2#001110#, 6);

  i_out_signed <= signed(i_out);

  -- <S10>/Data Type Conversion2
  
  Data_Type_Conversion2_out1 <= "011111111111" WHEN (i_out_signed(26) = '0') AND (i_out_signed(25 DOWNTO 23) /= "000") ELSE
      "100000000000" WHEN (i_out_signed(26) = '1') AND (i_out_signed(25 DOWNTO 23) /= "111") ELSE
      i_out_signed(23 DOWNTO 12);

  -- <S10>/Delay1
  Delay1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      i_in_2 <= to_signed(2#000000000000#, 12);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        i_in_2 <= Data_Type_Conversion2_out1;
      END IF;
    END IF;
  END PROCESS Delay1_process;


  Delay1_out_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      i_in_3 <= to_signed(2#000000000000#, 12);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        i_in_3 <= i_in_2;
      END IF;
    END IF;
  END PROCESS Delay1_out_pipe_process;


  out_0_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      i_in_4 <= to_signed(2#000000000000#, 12);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        i_in_4 <= i_in_3;
      END IF;
    END IF;
  END PROCESS out_0_pipe_process;


  dc_i_out <= std_logic_vector(i_in_4);

  q_out_signed <= signed(q_out);

  -- <S10>/Data Type Conversion3
  
  Data_Type_Conversion3_out1 <= "011111111111" WHEN (q_out_signed(26) = '0') AND (q_out_signed(25 DOWNTO 23) /= "000") ELSE
      "100000000000" WHEN (q_out_signed(26) = '1') AND (q_out_signed(25 DOWNTO 23) /= "111") ELSE
      q_out_signed(23 DOWNTO 12);

  -- <S10>/Delay2
  Delay2_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      q_in_2 <= to_signed(2#000000000000#, 12);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        q_in_2 <= Data_Type_Conversion3_out1;
      END IF;
    END IF;
  END PROCESS Delay2_process;


  Delay2_out_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      q_in_3 <= to_signed(2#000000000000#, 12);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        q_in_3 <= q_in_2;
      END IF;
    END IF;
  END PROCESS Delay2_out_pipe_process;


  out_1_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      q_in_4 <= to_signed(2#000000000000#, 12);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        q_in_4 <= q_in_3;
      END IF;
    END IF;
  END PROCESS out_1_pipe_process;


  dc_q_out <= std_logic_vector(q_in_4);

  rssi_out_unsigned <= unsigned(rssi_out);

  -- <S10>/Delay3
  Delay3_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      rssi_out_1 <= to_unsigned(16#000000#, 24);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        rssi_out_1 <= rssi_out_unsigned;
      END IF;
    END IF;
  END PROCESS Delay3_process;


  Delay3_out_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      rssi_out_2 <= to_unsigned(16#000000#, 24);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        rssi_out_2 <= rssi_out_1;
      END IF;
    END IF;
  END PROCESS Delay3_out_pipe_process;


  out_2_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      rssi_out_3 <= to_unsigned(16#000000#, 24);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        rssi_out_3 <= rssi_out_2;
      END IF;
    END IF;
  END PROCESS out_2_pipe_process;


  dc_rssi_out <= std_logic_vector(rssi_out_3);

  gain_out_unsigned <= unsigned(gain_out);

  -- <S10>/Delay7
  Delay7_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      gain_out_1 <= to_unsigned(2#000000#, 6);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        gain_out_1 <= gain_out_unsigned;
      END IF;
    END IF;
  END PROCESS Delay7_process;


  Delay7_out_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      gain_out_2 <= to_unsigned(2#000000#, 6);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        gain_out_2 <= gain_out_1;
      END IF;
    END IF;
  END PROCESS Delay7_out_pipe_process;


  out_3_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      gain_out_3 <= to_unsigned(2#000000#, 6);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        gain_out_3 <= gain_out_2;
      END IF;
    END IF;
  END PROCESS out_3_pipe_process;


  dc_gain_out <= std_logic_vector(gain_out_3);

  i_mean_out_signed <= signed(i_mean_out);

  -- <S10>/Delay8
  Delay8_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      i_mean_out_1 <= to_signed(16#000000#, 24);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        i_mean_out_1 <= i_mean_out_signed;
      END IF;
    END IF;
  END PROCESS Delay8_process;


  Delay8_out_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      i_mean_out_2 <= to_signed(16#000000#, 24);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        i_mean_out_2 <= i_mean_out_1;
      END IF;
    END IF;
  END PROCESS Delay8_out_pipe_process;


  out_4_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      i_mean_out_3 <= to_signed(16#000000#, 24);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        i_mean_out_3 <= i_mean_out_2;
      END IF;
    END IF;
  END PROCESS out_4_pipe_process;


  dc_i_mean_out <= std_logic_vector(i_mean_out_3);

  i_dc_out_signed <= signed(i_dc_out);

  -- <S10>/Delay9
  Delay9_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      i_dc_out_1 <= to_signed(16#000000#, 24);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        i_dc_out_1 <= i_dc_out_signed;
      END IF;
    END IF;
  END PROCESS Delay9_process;


  Delay9_out_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      i_dc_out_2 <= to_signed(16#000000#, 24);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        i_dc_out_2 <= i_dc_out_1;
      END IF;
    END IF;
  END PROCESS Delay9_out_pipe_process;


  out_5_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      i_dc_out_3 <= to_signed(16#000000#, 24);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        i_dc_out_3 <= i_dc_out_2;
      END IF;
    END IF;
  END PROCESS out_5_pipe_process;


  dc_i_dc_out <= std_logic_vector(i_dc_out_3);

  dir_out_unsigned <= unsigned(dir_out);

  -- <S10>/Delay5
  Delay5_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      dir_out_1 <= to_unsigned(2#0000#, 4);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        dir_out_1 <= dir_out_unsigned;
      END IF;
    END IF;
  END PROCESS Delay5_process;


  Delay5_out_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      dir_out_2 <= to_unsigned(2#0000#, 4);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        dir_out_2 <= dir_out_1;
      END IF;
    END IF;
  END PROCESS Delay5_out_pipe_process;


  out_6_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      dir_out_3 <= to_unsigned(2#0000#, 4);
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        dir_out_3 <= dir_out_2;
      END IF;
    END IF;
  END PROCESS out_6_pipe_process;


  dc_dir_out <= std_logic_vector(dir_out_3);

  -- <S10>/Delay6
  Delay6_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      blinky_1 <= '0';
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        blinky_1 <= blinky;
      END IF;
    END IF;
  END PROCESS Delay6_process;


  Delay6_out_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      blinky_2 <= '0';
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        blinky_2 <= blinky_1;
      END IF;
    END IF;
  END PROCESS Delay6_out_pipe_process;


  out_7_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      blinky_3 <= '0';
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        blinky_3 <= blinky_2;
      END IF;
    END IF;
  END PROCESS out_7_pipe_process;


  ce_out <= clk_enable;

  -- <S10>/Scope1

  dc_blinky <= blinky_3;

END rtl;

