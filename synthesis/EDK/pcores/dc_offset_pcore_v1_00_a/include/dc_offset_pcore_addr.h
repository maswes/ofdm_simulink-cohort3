/*
 * File Name:         C:\temp\simulink_code_gen\hdl_prj\ipcore\dc_offset_pcore_v1_00_a\include\dc_offset_pcore_addr.h
 * Description:       C Header File
 * Created:           2015-05-30 21:50:22
*/

#ifndef DC_OFFSET_PCORE_H_
#define DC_OFFSET_PCORE_H_

#define  IPCore_Reset_dc_offset_pcore    0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_dc_offset_pcore   0x4  //enabled (by default) when bit 0 is 0x1

#endif /* DC_OFFSET_PCORE_H_ */
