-- -------------------------------------------------------------
-- 
-- File Name: C:\temp\simulink_code_gen\hdl_prj\hdlsrc\phy_layer58_input_sync_attempt\upsample_8_real_stream.vhd
-- Created: 2015-06-04 23:50:26
-- 
-- Generated by MATLAB 8.3 and HDL Coder 3.4
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: upsample_8_real_stream
-- Source Path: phy_layer58_input_sync_attempt/tx_lower/tx_upsample/upsample_8_real_stream
-- Hierarchy Level: 2
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY upsample_8_real_stream IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        enb_1_2_0                         :   IN    std_logic;
        idata                             :   IN    std_logic_vector(15 DOWNTO 0);  -- sfix16_En15
        ivalid                            :   IN    std_logic;  -- ufix1
        oready                            :   IN    std_logic;  -- ufix1
        odata                             :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En15
        ovalid                            :   OUT   std_logic;  -- ufix1
        iready                            :   OUT   std_logic  -- ufix1
        );
END upsample_8_real_stream;


ARCHITECTURE rtl OF upsample_8_real_stream IS

  -- Signals
  SIGNAL idata_signed                     : signed(15 DOWNTO 0);  -- sfix16_En15
  SIGNAL odata_tmp                        : signed(15 DOWNTO 0);  -- sfix16_En15
  SIGNAL count                            : unsigned(7 DOWNTO 0);  -- uint8
  SIGNAL odata_hold                       : signed(15 DOWNTO 0);  -- sfix16
  SIGNAL count_next                       : unsigned(7 DOWNTO 0);  -- uint8
  SIGNAL odata_hold_next                  : signed(15 DOWNTO 0);  -- sfix16_En15

BEGIN
  idata_signed <= signed(idata);

  mylib_c23_upsample_8_real_stream_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      count <= (OTHERS => '0');
      odata_hold <= (OTHERS => '0');
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        count <= count_next;
        odata_hold <= odata_hold_next;
      END IF;
    END IF;
  END PROCESS mylib_c23_upsample_8_real_stream_process;

  mylib_c23_upsample_8_real_stream_output : PROCESS (idata_signed, ivalid, oready, count, odata_hold)
    VARIABLE add_temp : unsigned(8 DOWNTO 0);
  BEGIN
    count_next <= count;
    odata_hold_next <= odata_hold;
    -- Drive the output signals

    CASE count IS
      WHEN "00000000" =>
        odata_tmp <= idata_signed;
        odata_hold_next <= idata_signed;
        iready <= '0';
      WHEN "00000111" =>
        odata_tmp <= odata_hold;
        iready <= oready;
      WHEN OTHERS => 
        odata_tmp <= odata_hold;
        iready <= '0';
    END CASE;

    ovalid <= ivalid;
    -- Update the counter
    -- switch count
    --     case 0
    --         if ivalid == 1
    --             count = count +1;
    --         end
    --     case 7
    --         count = cast(0, 'like', count);    
    --     otherwise
    --         count = count + 1;
    -- end
    -- Update the counter
    IF (ivalid = '1') AND (oready = '1') THEN 
      IF count = 7 THEN 
        count_next <= (OTHERS => '0');
      ELSE 
        add_temp := resize(count, 9) + 1;
        IF add_temp(8) /= '0' THEN 
          count_next <= (OTHERS => '1');
        ELSE 
          count_next <= add_temp(7 DOWNTO 0);
        END IF;
      END IF;
    END IF;
  END PROCESS mylib_c23_upsample_8_real_stream_output;


  odata <= std_logic_vector(odata_tmp);

END rtl;

