/*
 * File Name:         C:\temp\simulink_code_gen\hdl_prj\ipcore\tx_lower_pcore_v1_00_a\include\tx_lower_pcore_addr.h
 * Description:       C Header File
 * Created:           2015-06-04 23:50:28
*/

#ifndef TX_LOWER_PCORE_H_
#define TX_LOWER_PCORE_H_

#define  IPCore_Reset_tx_lower_pcore    0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_tx_lower_pcore   0x4  //enabled (by default) when bit 0 is 0x1

#endif /* TX_LOWER_PCORE_H_ */
