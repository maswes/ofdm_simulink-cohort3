
-------------------------------------------------------------------
-- System Generator version 14.6 VHDL source file.
--
-- Copyright(C) 2013 by Xilinx, Inc.  All rights reserved.  This
-- text/file contains proprietary, confidential information of Xilinx,
-- Inc., is distributed under license from Xilinx, Inc., and may be used,
-- copied and/or disclosed only pursuant to the terms of a valid license
-- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
-- this text/file solely for design, simulation, implementation and
-- creation of design files limited to Xilinx devices or technologies.
-- Use with non-Xilinx devices or technologies is expressly prohibited
-- and immediately terminates your license unless covered by a separate
-- agreement.
--
-- Xilinx is providing this design, code, or information "as is" solely
-- for use in developing programs and solutions for Xilinx devices.  By
-- providing this design, code, or information as one possible
-- implementation of this feature, application or standard, Xilinx is
-- making no representation that this implementation is free from any
-- claims of infringement.  You are responsible for obtaining any rights
-- you may require for your implementation.  Xilinx expressly disclaims
-- any warranty whatsoever with respect to the adequacy of the
-- implementation, including but not limited to warranties of
-- merchantability or fitness for a particular purpose.
--
-- Xilinx products are not intended for use in life support appliances,
-- devices, or systems.  Use in such applications is expressly prohibited.
--
-- Any modifications that are made to the source code are done at the user's
-- sole risk and will be unsupported.
--
-- This copyright and support notice must be retained as part of this
-- text at all times.  (c) Copyright 1995-2013 Xilinx, Inc.  All rights
-- reserved.
-------------------------------------------------------------------
library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

entity tx_ifft_buf_wrap_mcw is
  port (
    clk_1: in std_logic; -- clock period = 50.0 ns (20.0 Mhz)
    ifft_aresetn: in std_logic; 
    ifft_idata_imag: in std_logic_vector(15 downto 0); 
    ifft_idata_last: in std_logic; 
    ifft_idata_real: in std_logic_vector(15 downto 0); 
    ifft_idata_valid: in std_logic; 
    ifft_odata_ready: in std_logic; 
    tx_fifo_aresetn: in std_logic; 
    tx_fifo_idata_imag: in std_logic_vector(15 downto 0); 
    tx_fifo_idata_real: in std_logic_vector(15 downto 0); 
    tx_fifo_ivalid: in std_logic; 
    tx_fifo_last: in std_logic; 
    tx_fifo_oready: in std_logic; 
    ifft_cfg_ready: out std_logic; 
    ifft_idata_ready: out std_logic; 
    ifft_odata_imag: out std_logic_vector(15 downto 0); 
    ifft_odata_last: out std_logic; 
    ifft_odata_real: out std_logic_vector(15 downto 0); 
    ifft_odata_valid: out std_logic; 
    ifft_status: out std_logic_vector(5 downto 0); 
    tx_fifo_data_count: out std_logic_vector(6 downto 0); 
    tx_fifo_idata_ready: out std_logic; 
    tx_fifo_odata_imag: out std_logic_vector(15 downto 0); 
    tx_fifo_odata_real: out std_logic_vector(15 downto 0); 
    tx_fifo_olast: out std_logic; 
    tx_fifo_ovalid: out std_logic
  );
end tx_ifft_buf_wrap_mcw;

architecture structural of tx_ifft_buf_wrap_mcw is
  signal clk_1_sg_x2: std_logic;
  signal ifft_aresetn_net: std_logic;
  signal ifft_cfg_ready_net: std_logic;
  signal ifft_idata_imag_net: std_logic_vector(15 downto 0);
  signal ifft_idata_last_net: std_logic;
  signal ifft_idata_ready_net: std_logic;
  signal ifft_idata_real_net: std_logic_vector(15 downto 0);
  signal ifft_idata_valid_net: std_logic;
  signal ifft_odata_imag_net: std_logic_vector(15 downto 0);
  signal ifft_odata_last_net: std_logic;
  signal ifft_odata_ready_net: std_logic;
  signal ifft_odata_real_net: std_logic_vector(15 downto 0);
  signal ifft_odata_valid_net: std_logic;
  signal ifft_status_net: std_logic_vector(5 downto 0);
  signal tx_fifo_aresetn_net: std_logic;
  signal tx_fifo_data_count_net: std_logic_vector(6 downto 0);
  signal tx_fifo_idata_imag_net: std_logic_vector(15 downto 0);
  signal tx_fifo_idata_ready_net: std_logic;
  signal tx_fifo_idata_real_net: std_logic_vector(15 downto 0);
  signal tx_fifo_ivalid_net: std_logic;
  signal tx_fifo_last_net: std_logic;
  signal tx_fifo_odata_imag_net: std_logic_vector(15 downto 0);
  signal tx_fifo_odata_real_net: std_logic_vector(15 downto 0);
  signal tx_fifo_olast_net: std_logic;
  signal tx_fifo_oready_net: std_logic;
  signal tx_fifo_ovalid_net: std_logic;

begin
  clk_1_sg_x2 <= clk_1;
  ifft_aresetn_net <= ifft_aresetn;
  ifft_idata_imag_net <= ifft_idata_imag;
  ifft_idata_last_net <= ifft_idata_last;
  ifft_idata_real_net <= ifft_idata_real;
  ifft_idata_valid_net <= ifft_idata_valid;
  ifft_odata_ready_net <= ifft_odata_ready;
  tx_fifo_aresetn_net <= tx_fifo_aresetn;
  tx_fifo_idata_imag_net <= tx_fifo_idata_imag;
  tx_fifo_idata_real_net <= tx_fifo_idata_real;
  tx_fifo_ivalid_net <= tx_fifo_ivalid;
  tx_fifo_last_net <= tx_fifo_last;
  tx_fifo_oready_net <= tx_fifo_oready;
  ifft_cfg_ready <= ifft_cfg_ready_net;
  ifft_idata_ready <= ifft_idata_ready_net;
  ifft_odata_imag <= ifft_odata_imag_net;
  ifft_odata_last <= ifft_odata_last_net;
  ifft_odata_real <= ifft_odata_real_net;
  ifft_odata_valid <= ifft_odata_valid_net;
  ifft_status <= ifft_status_net;
  tx_fifo_data_count <= tx_fifo_data_count_net;
  tx_fifo_idata_ready <= tx_fifo_idata_ready_net;
  tx_fifo_odata_imag <= tx_fifo_odata_imag_net;
  tx_fifo_odata_real <= tx_fifo_odata_real_net;
  tx_fifo_olast <= tx_fifo_olast_net;
  tx_fifo_ovalid <= tx_fifo_ovalid_net;

  tx_ifft_buf_wrap_x0: entity work.tx_ifft_buf_wrap
    port map (
      ce_1 => '1',
      clk_1 => clk_1_sg_x2,
      ifft_aresetn => ifft_aresetn_net,
      ifft_idata_imag => ifft_idata_imag_net,
      ifft_idata_last => ifft_idata_last_net,
      ifft_idata_real => ifft_idata_real_net,
      ifft_idata_valid => ifft_idata_valid_net,
      ifft_odata_ready => ifft_odata_ready_net,
      tx_fifo_aresetn => tx_fifo_aresetn_net,
      tx_fifo_idata_imag => tx_fifo_idata_imag_net,
      tx_fifo_idata_real => tx_fifo_idata_real_net,
      tx_fifo_ivalid => tx_fifo_ivalid_net,
      tx_fifo_last => tx_fifo_last_net,
      tx_fifo_oready => tx_fifo_oready_net,
      ifft_cfg_ready => ifft_cfg_ready_net,
      ifft_idata_ready => ifft_idata_ready_net,
      ifft_odata_imag => ifft_odata_imag_net,
      ifft_odata_last => ifft_odata_last_net,
      ifft_odata_real => ifft_odata_real_net,
      ifft_odata_valid => ifft_odata_valid_net,
      ifft_status => ifft_status_net,
      tx_fifo_data_count => tx_fifo_data_count_net,
      tx_fifo_idata_ready => tx_fifo_idata_ready_net,
      tx_fifo_odata_imag => tx_fifo_odata_imag_net,
      tx_fifo_odata_real => tx_fifo_odata_real_net,
      tx_fifo_olast => tx_fifo_olast_net,
      tx_fifo_ovalid => tx_fifo_ovalid_net
    );

end structural;
