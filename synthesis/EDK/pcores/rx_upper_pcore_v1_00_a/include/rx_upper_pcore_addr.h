/*
 * File Name:         C:\temp\simulink_code_gen\hdl_prj\ipcore\rx_upper_pcore_v1_00_a\include\rx_upper_pcore_addr.h
 * Description:       C Header File
 * Created:           2015-06-08 06:32:02
*/

#ifndef RX_UPPER_PCORE_H_
#define RX_UPPER_PCORE_H_

#define  IPCore_Reset_rx_upper_pcore    0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_rx_upper_pcore   0x4  //enabled (by default) when bit 0 is 0x1

#endif /* RX_UPPER_PCORE_H_ */
