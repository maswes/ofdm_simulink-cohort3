-- -------------------------------------------------------------
-- 
-- File Name: C:\temp\simulink_code_gen\hdl_prj\hdlsrc\phy_layer59_delayed_sync\upsample_2_real1.vhd
-- Created: 2015-06-08 06:31:58
-- 
-- Generated by MATLAB 8.3 and HDL Coder 3.4
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: upsample_2_real1
-- Source Path: phy_layer59_delayed_sync/rx_upper/qpsk_decode_wrap/upsample_2_real1
-- Hierarchy Level: 2
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY upsample_2_real1 IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        enb                               :   IN    std_logic;
        u_re                              :   IN    std_logic_vector(15 DOWNTO 0);  -- sfix16_En15
        u_im                              :   IN    std_logic_vector(15 DOWNTO 0);  -- sfix16_En15
        y_re                              :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En15
        y_im                              :   OUT   std_logic_vector(15 DOWNTO 0)  -- sfix16_En15
        );
END upsample_2_real1;


ARCHITECTURE rtl OF upsample_2_real1 IS

  -- Signals
  SIGNAL u_re_signed                      : signed(15 DOWNTO 0);  -- sfix16_En15
  SIGNAL u_im_signed                      : signed(15 DOWNTO 0);  -- sfix16_En15
  SIGNAL y_re_tmp                         : signed(15 DOWNTO 0);  -- sfix16_En15
  SIGNAL y_im_tmp                         : signed(15 DOWNTO 0);  -- sfix16_En15
  SIGNAL count                            : unsigned(7 DOWNTO 0);  -- uint8
  SIGNAL y_hold_re                        : signed(15 DOWNTO 0);  -- sfix16_En15
  SIGNAL y_hold_im                        : signed(15 DOWNTO 0);  -- sfix16_En15
  SIGNAL count_next                       : unsigned(7 DOWNTO 0);  -- uint8
  SIGNAL y_hold_next_re                   : signed(15 DOWNTO 0);  -- sfix16_En15
  SIGNAL y_hold_next_im                   : signed(15 DOWNTO 0);  -- sfix16_En15

BEGIN
  u_re_signed <= signed(u_re);

  u_im_signed <= signed(u_im);

  upsample_2_real1_1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      count <= (OTHERS => '0');
      y_hold_re <= (OTHERS => '0');
      y_hold_im <= (OTHERS => '0');
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' THEN
        count <= count_next;
        y_hold_re <= y_hold_next_re;
        y_hold_im <= y_hold_next_im;
      END IF;
    END IF;
  END PROCESS upsample_2_real1_1_process;

  upsample_2_real1_1_output : PROCESS (u_re_signed, u_im_signed, count, y_hold_re, y_hold_im)
    VARIABLE add_temp : unsigned(8 DOWNTO 0);
  BEGIN
    count_next <= count;
    y_hold_next_re <= y_hold_re;
    y_hold_next_im <= y_hold_im;

    CASE count IS
      WHEN "00000000" =>
        y_re_tmp <= u_re_signed;
        y_im_tmp <= u_im_signed;
        y_hold_next_re <= u_re_signed;
        y_hold_next_im <= u_im_signed;
      WHEN OTHERS => 
        y_re_tmp <= y_hold_re;
        y_im_tmp <= y_hold_im;
    END CASE;

    IF count = 1 THEN 
      count_next <= (OTHERS => '0');
    ELSE 
      add_temp := resize(count, 9) + 1;
      IF add_temp(8) /= '0' THEN 
        count_next <= (OTHERS => '1');
      ELSE 
        count_next <= add_temp(7 DOWNTO 0);
      END IF;
    END IF;
  END PROCESS upsample_2_real1_1_output;


  y_re <= std_logic_vector(y_re_tmp);

  y_im <= std_logic_vector(y_im_tmp);

END rtl;

