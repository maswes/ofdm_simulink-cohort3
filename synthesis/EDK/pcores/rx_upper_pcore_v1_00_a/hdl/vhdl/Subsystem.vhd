-- -------------------------------------------------------------
-- 
-- File Name: C:\temp\simulink_code_gen\hdl_prj\hdlsrc\phy_layer59_delayed_sync\Subsystem.vhd
-- Created: 2015-06-08 06:31:58
-- 
-- Generated by MATLAB 8.3 and HDL Coder 3.4
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: Subsystem
-- Source Path: phy_layer59_delayed_sync/rx_upper/descramble_wrap/Subsystem
-- Hierarchy Level: 2
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY Subsystem IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        enb_1_4_0                         :   IN    std_logic;
        In1                               :   IN    std_logic;  -- ufix1
        In2                               :   IN    std_logic;  -- ufix1
        Enable                            :   IN    std_logic;  -- ufix1
        Out1                              :   OUT   std_logic  -- ufix1
        );
END Subsystem;


ARCHITECTURE rtl OF Subsystem IS

  -- Component Declarations
  COMPONENT MATLAB_Function
    PORT( clk                             :   IN    std_logic;
          reset                           :   IN    std_logic;
          enb_1_4_0                       :   IN    std_logic;
          idata                           :   IN    std_logic_vector(7 DOWNTO 0);  -- uint8
          odata                           :   OUT   std_logic_vector(7 DOWNTO 0)  -- uint8
          );
  END COMPONENT;

  -- Component Configuration Statements
  FOR ALL : MATLAB_Function
    USE ENTITY work.MATLAB_Function(rtl);

  -- Signals
  SIGNAL enb_1_4_0_gated                  : std_logic;
  SIGNAL ivalid                           : std_logic;  -- ufix1
  SIGNAL Data_Type_Conversion1_out1       : unsigned(7 DOWNTO 0);  -- uint8
  SIGNAL switch_compare_1                 : std_logic;
  SIGNAL odata                            : std_logic_vector(7 DOWNTO 0);  -- ufix8
  SIGNAL odata_unsigned                   : unsigned(7 DOWNTO 0);  -- uint8
  SIGNAL Data_Type_Conversion_out1        : std_logic;  -- ufix1
  SIGNAL odata_1                          : std_logic;  -- ufix1
  SIGNAL odata_last_value                 : std_logic;  -- ufix1
  SIGNAL odata_bypass                     : std_logic;  -- ufix1

BEGIN
  u_MATLAB_Function : MATLAB_Function
    PORT MAP( clk => clk,
              reset => reset,
              enb_1_4_0 => enb_1_4_0_gated,
              idata => std_logic_vector(Data_Type_Conversion1_out1),  -- uint8
              odata => odata  -- uint8
              );

  ivalid <= Enable;

  Data_Type_Conversion1_out1 <= '0' & '0' & '0' & '0' & '0' & '0' & '0' & In1;

  enb_1_4_0_gated <= ivalid AND enb_1_4_0;

  
  switch_compare_1 <= '1' WHEN In2 > '0' ELSE
      '0';

  odata_unsigned <= unsigned(odata);

  Data_Type_Conversion_out1 <= odata_unsigned(0);

  
  odata_1 <= Data_Type_Conversion_out1 WHEN switch_compare_1 = '0' ELSE
      In1;

  Out1_bypass_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      odata_last_value <= '0';
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_4_0_gated = '1' THEN
        odata_last_value <= odata_1;
      END IF;
    END IF;
  END PROCESS Out1_bypass_process;


  
  odata_bypass <= odata_last_value WHEN ivalid = '0' ELSE
      odata_1;

  Out1 <= odata_bypass;

END rtl;

