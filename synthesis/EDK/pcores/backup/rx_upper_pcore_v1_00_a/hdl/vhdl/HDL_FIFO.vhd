-- -------------------------------------------------------------
-- 
-- File Name: C:\temp\simulink_code_gen\hdl_prj\hdlsrc\phy_layer59_delayed_sync\HDL_FIFO.vhd
-- Created: 2015-06-07 22:32:37
-- 
-- Generated by MATLAB 8.3 and HDL Coder 3.4
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: HDL_FIFO
-- Source Path: phy_layer59_delayed_sync/rx_upper/deserialize_1:4/HDL FIFO
-- Hierarchy Level: 2
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY HDL_FIFO IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        enb                               :   IN    std_logic;
        enb_1_4_1                         :   IN    std_logic;
        enb_1_1_1                         :   IN    std_logic;
        In_rsvd                           :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
        Push                              :   IN    std_logic;  -- ufix1
        Pop                               :   IN    std_logic;  -- ufix1
        Out_rsvd                          :   OUT   std_logic_vector(11 DOWNTO 0);  -- ufix12
        Empty                             :   OUT   std_logic;  -- ufix1
        Full                              :   OUT   std_logic;  -- ufix1
        Num                               :   OUT   std_logic_vector(3 DOWNTO 0)  -- ufix4
        );
END HDL_FIFO;


ARCHITECTURE rtl OF HDL_FIFO IS

  -- Component Declarations
  COMPONENT HDL_FIFO_ram
    PORT( clk                             :   IN    std_logic;
          enb                             :   IN    std_logic;
          wr_din                          :   IN    std_logic_vector(11 DOWNTO 0);  -- ufix12
          wr_addr                         :   IN    std_logic_vector(3 DOWNTO 0);  -- ufix4
          wr_en                           :   IN    std_logic;  -- ufix1
          rd_addr                         :   IN    std_logic_vector(3 DOWNTO 0);  -- ufix4
          rd_dout                         :   OUT   std_logic_vector(11 DOWNTO 0)  -- ufix12
          );
  END COMPONENT;

  -- Component Configuration Statements
  FOR ALL : HDL_FIFO_ram
    USE ENTITY work.HDL_FIFO_ram(rtl);

  -- Signals
  SIGNAL us3_zero                         : std_logic;  -- ufix1
  SIGNAL us3_muxout                       : std_logic;  -- ufix1
  SIGNAL us3_bypass_reg                   : std_logic;  -- ufix1
  SIGNAL us3_bypassout                    : std_logic;  -- ufix1
  SIGNAL fifo_front_indx                  : unsigned(3 DOWNTO 0);  -- ufix4
  SIGNAL fifo_front_dir                   : unsigned(3 DOWNTO 0);  -- ufix4
  SIGNAL fifo_back_indx                   : unsigned(3 DOWNTO 0);  -- ufix4
  SIGNAL fifo_back_dir                    : unsigned(3 DOWNTO 0);  -- ufix4
  SIGNAL fifo_sample_count                : unsigned(3 DOWNTO 0);  -- ufix4
  SIGNAL fifo_front_indx_next             : unsigned(3 DOWNTO 0);  -- ufix4
  SIGNAL fifo_front_dir_next              : unsigned(3 DOWNTO 0);  -- ufix4
  SIGNAL fifo_back_indx_next              : unsigned(3 DOWNTO 0);  -- ufix4
  SIGNAL fifo_back_dir_next               : unsigned(3 DOWNTO 0);  -- ufix4
  SIGNAL fifo_sample_count_next           : unsigned(3 DOWNTO 0);  -- ufix4
  SIGNAL fifo_out3                        : std_logic;
  SIGNAL fifo_out4                        : std_logic;
  SIGNAL fifo_out1                        : std_logic;
  SIGNAL fifo_1                           : std_logic;
  SIGNAL fifo_front_indx_temp             : unsigned(3 DOWNTO 0);  -- ufix4
  SIGNAL fifo_back_indx_temp              : unsigned(3 DOWNTO 0);  -- ufix4
  SIGNAL w_waddr                          : unsigned(3 DOWNTO 0);  -- ufix4
  SIGNAL w_we                             : std_logic;  -- ufix1
  SIGNAL w_raddr                          : unsigned(3 DOWNTO 0);  -- ufix4
  SIGNAL Num_tmp                          : unsigned(3 DOWNTO 0);  -- ufix4
  SIGNAL w_cz                             : std_logic;
  SIGNAL w_const                          : unsigned(11 DOWNTO 0);  -- ufix12
  SIGNAL w_us3_dtc                        : unsigned(11 DOWNTO 0);  -- ufix12
  SIGNAL w_mux1                           : unsigned(11 DOWNTO 0);  -- ufix12
  SIGNAL w_d1                             : unsigned(11 DOWNTO 0);  -- ufix12
  SIGNAL w_waddr_1                        : std_logic_vector(11 DOWNTO 0);  -- ufix12
  SIGNAL w_waddr_unsigned                 : unsigned(11 DOWNTO 0);  -- ufix12
  SIGNAL f_d2_enable                      : std_logic;
  SIGNAL w_d2                             : unsigned(11 DOWNTO 0);  -- ufix12
  SIGNAL w_out                            : unsigned(11 DOWNTO 0);  -- ufix12
  SIGNAL downsample_bypass_reg            : unsigned(11 DOWNTO 0);  -- ufix12
  SIGNAL Out_tmp                          : unsigned(11 DOWNTO 0);  -- ufix12

BEGIN
  -- us2: Upsample by 1, Sample offset 0 
  -- 
  -- us1: Upsample by 1, Sample offset 0 
  u_HDL_FIFO_ram : HDL_FIFO_ram
    PORT MAP( clk => clk,
              enb => enb,
              wr_din => In_rsvd,  -- ufix12
              wr_addr => std_logic_vector(w_waddr),  -- ufix4
              wr_en => w_we,  -- ufix1
              rd_addr => std_logic_vector(w_raddr),  -- ufix4
              rd_dout => w_waddr_1  -- ufix12
              );

  -- us3: Upsample by 4, Sample offset 0 
  us3_zero <= '0';

  
  us3_muxout <= Pop WHEN enb_1_4_1 = '1' ELSE
      us3_zero;

  -- Upsample bypass register
  us3_bypass_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      us3_bypass_reg <= '0';
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_1_1 = '1' THEN
        us3_bypass_reg <= us3_muxout;
      END IF;
    END IF;
  END PROCESS us3_bypass_process;

  
  us3_bypassout <= us3_muxout WHEN enb_1_1_1 = '1' ELSE
      us3_bypass_reg;

  -- FIFO logic controller
  fifo_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      fifo_front_indx <= (OTHERS => '0');
      fifo_front_dir <= (0 => '1', OTHERS => '0');
      fifo_back_indx <= (OTHERS => '0');
      fifo_back_dir <= (0 => '1', OTHERS => '0');
      fifo_sample_count <= (OTHERS => '0');
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' THEN
        fifo_front_indx <= fifo_front_indx_next;
        fifo_front_dir <= fifo_front_dir_next;
        fifo_back_indx <= fifo_back_indx_next;
        fifo_back_dir <= fifo_back_dir_next;
        fifo_sample_count <= fifo_sample_count_next;
      END IF;
    END IF;
  END PROCESS fifo_process;

  
  fifo_out4 <= '1' WHEN fifo_sample_count = 10 ELSE
      '0';
  
  fifo_out3 <= '1' WHEN fifo_sample_count = 0 ELSE
      '0';
  
  fifo_out1 <= '1' WHEN (Push AND (us3_bypassout OR ( NOT fifo_out4))) = '1' ELSE
      '0';
  
  fifo_1 <= '1' WHEN (us3_bypassout AND ( NOT fifo_out3)) = '1' ELSE
      '0';
  
  fifo_front_indx_temp <= fifo_front_indx + fifo_front_dir WHEN fifo_1 = '1' ELSE
      fifo_front_indx;
  
  fifo_front_dir_next <= to_unsigned(2#0111#, 4) WHEN fifo_front_indx_temp = 9 ELSE
      to_unsigned(2#0001#, 4);
  
  fifo_back_indx_temp <= fifo_back_indx + fifo_back_dir WHEN fifo_out1 = '1' ELSE
      fifo_back_indx;
  
  fifo_back_dir_next <= to_unsigned(2#0111#, 4) WHEN fifo_back_indx_temp = 9 ELSE
      to_unsigned(2#0001#, 4);
  
  fifo_sample_count_next <= fifo_sample_count + 1 WHEN (fifo_out1 AND ( NOT fifo_1)) = '1' ELSE
      fifo_sample_count + 15 WHEN (( NOT fifo_out1) AND fifo_1) = '1' ELSE
      fifo_sample_count;
  w_waddr <= fifo_back_indx;
  w_we <= fifo_out1;
  w_raddr <= fifo_front_indx;
  Empty <= fifo_out3;
  Full <= fifo_out4;
  Num_tmp <= fifo_sample_count;
  fifo_front_indx_next <= fifo_front_indx_temp;
  fifo_back_indx_next <= fifo_back_indx_temp;

  
  w_cz <= '1' WHEN Num_tmp > 0 ELSE
      '0';

  w_const <= (OTHERS => '0');

  w_us3_dtc <= '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & '0' & us3_bypassout;

  
  w_mux1 <= w_const WHEN w_cz = '0' ELSE
      w_us3_dtc;

  f_d1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      w_d1 <= (OTHERS => '0');
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' THEN
        w_d1 <= w_mux1;
      END IF;
    END IF;
  END PROCESS f_d1_process;


  w_waddr_unsigned <= unsigned(w_waddr_1);

  
  f_d2_enable <= '1' WHEN w_d1 /= 0 ELSE
      '0';

  f_d2_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      w_d2 <= (OTHERS => '0');
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb = '1' AND f_d2_enable = '1' THEN
        w_d2 <= w_waddr_unsigned;
      END IF;
    END IF;
  END PROCESS f_d2_process;


  
  w_out <= w_d2 WHEN w_d1 = 0 ELSE
      w_waddr_unsigned;

  -- downsample: Downsample by 4, Sample offset 0 
  -- Downsample bypass register
  downsample_bypass_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      downsample_bypass_reg <= (OTHERS => '0');
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_4_1 = '1' THEN
        downsample_bypass_reg <= w_out;
      END IF;
    END IF;
  END PROCESS downsample_bypass_process;

  
  Out_tmp <= w_out WHEN enb_1_4_1 = '1' ELSE
      downsample_bypass_reg;

  Out_rsvd <= std_logic_vector(Out_tmp);

  Num <= std_logic_vector(Num_tmp);

END rtl;

