/*
 * File Name:         C:\temp\simulink_code_gen\hdl_prj\ipcore\rx_lower_pcore_v1_00_a\include\rx_lower_pcore_addr.h
 * Description:       C Header File
 * Created:           2015-06-07 22:17:38
*/

#ifndef RX_LOWER_PCORE_H_
#define RX_LOWER_PCORE_H_

#define  IPCore_Reset_rx_lower_pcore            0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_rx_lower_pcore           0x4  //enabled (by default) when bit 0 is 0x1
#define  rx_lower_led_en_Data_rx_lower_pcore    0x100  //data register for port rx_lower_led_en
#define  test_dc_gain_out_Data_rx_lower_pcore   0x104  //data register for port test_dc_gain_out
#define  test_correlation_Data_rx_lower_pcore   0x108  //data register for port test_correlation

#endif /* RX_LOWER_PCORE_H_ */
