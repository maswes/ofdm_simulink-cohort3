-- -------------------------------------------------------------
-- 
-- File Name: C:\temp\simulink_code_gen\hdl_prj\hdlsrc\phy_layer59_delayed_sync\time_sync.vhd
-- Created: 2015-06-07 22:15:03
-- 
-- Generated by MATLAB 8.3 and HDL Coder 3.4
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: time_sync
-- Source Path: phy_layer59_delayed_sync/rx_lower/rx_lower_without_dc/time_sync
-- Hierarchy Level: 2
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY time_sync IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        enb_1_16_0                        :   IN    std_logic;
        enb_1_2_0                         :   IN    std_logic;
        enb_1_16_1                        :   IN    std_logic;
        i_data_re                         :   IN    std_logic_vector(15 DOWNTO 0);  -- sfix16_En3
        i_data_im                         :   IN    std_logic_vector(15 DOWNTO 0);  -- sfix16_En3
        o_data_re                         :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En3
        o_data_im                         :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En3
        correlation                       :   OUT   std_logic_vector(31 DOWNTO 0);  -- int32
        sync_out                          :   OUT   std_logic
        );
END time_sync;


ARCHITECTURE rtl OF time_sync IS

  -- Component Declarations
  COMPONENT ofdm_symb_timing_stream
    PORT( clk                             :   IN    std_logic;
          reset                           :   IN    std_logic;
          enb_1_16_0                      :   IN    std_logic;
          enb_1_2_0                       :   IN    std_logic;
          enb_1_16_1                      :   IN    std_logic;
          idata_re                        :   IN    std_logic_vector(15 DOWNTO 0);  -- sfix16_En3
          idata_im                        :   IN    std_logic_vector(15 DOWNTO 0);  -- sfix16_En3
          correlation                     :   OUT   std_logic_vector(31 DOWNTO 0);  -- int32
          sync                            :   OUT   std_logic;
          odata_re                        :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En3
          odata_im                        :   OUT   std_logic_vector(15 DOWNTO 0)  -- sfix16_En3
          );
  END COMPONENT;

  -- Component Configuration Statements
  FOR ALL : ofdm_symb_timing_stream
    USE ENTITY work.ofdm_symb_timing_stream(rtl);

  -- Signals
  SIGNAL ofdm_symb_timing_stream_out1     : std_logic_vector(31 DOWNTO 0);  -- ufix32
  SIGNAL ofdm_symb_timing_stream_out2     : std_logic;
  SIGNAL ofdm_symb_timing_stream_out3_re  : std_logic_vector(15 DOWNTO 0);  -- ufix16
  SIGNAL ofdm_symb_timing_stream_out3_im  : std_logic_vector(15 DOWNTO 0);  -- ufix16

BEGIN
  u_ofdm_symb_timing_stream : ofdm_symb_timing_stream
    PORT MAP( clk => clk,
              reset => reset,
              enb_1_16_0 => enb_1_16_0,
              enb_1_2_0 => enb_1_2_0,
              enb_1_16_1 => enb_1_16_1,
              idata_re => i_data_re,  -- sfix16_En3
              idata_im => i_data_im,  -- sfix16_En3
              correlation => ofdm_symb_timing_stream_out1,  -- int32
              sync => ofdm_symb_timing_stream_out2,
              odata_re => ofdm_symb_timing_stream_out3_re,  -- sfix16_En3
              odata_im => ofdm_symb_timing_stream_out3_im  -- sfix16_En3
              );

  o_data_re <= ofdm_symb_timing_stream_out3_re;

  o_data_im <= ofdm_symb_timing_stream_out3_im;

  correlation <= ofdm_symb_timing_stream_out1;

  sync_out <= ofdm_symb_timing_stream_out2;

END rtl;

