/*
 * File Name:         hdl_prj\tx_source\ipcore\tx_source_pcore_v1_00_a\include\tx_source_pcore_addr.h
 * Description:       C Header File
 * Created:           2015-05-19 23:00:48
*/

#ifndef TX_SOURCE_PCORE_H_
#define TX_SOURCE_PCORE_H_

#define  IPCore_Reset_tx_source_pcore    0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_tx_source_pcore   0x4  //enabled (by default) when bit 0 is 0x1

#endif /* TX_SOURCE_PCORE_H_ */
