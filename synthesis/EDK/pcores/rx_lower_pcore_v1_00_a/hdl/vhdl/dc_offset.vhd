-- -------------------------------------------------------------
-- 
-- File Name: C:\temp\simulink_code_gen\hdl_prj\hdlsrc\phy_layer59_delayed_sync\dc_offset.vhd
-- Created: 2015-06-08 00:52:40
-- 
-- Generated by MATLAB 8.3 and HDL Coder 3.4
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: dc_offset
-- Source Path: phy_layer59_delayed_sync/rx_lower/dc_offset
-- Hierarchy Level: 1
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY dc_offset IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        enb_1_2_0                         :   IN    std_logic;
        gain_in                           :   IN    std_logic_vector(11 DOWNTO 0);  -- sfix12_En6
        dc_dig_i_in1                      :   IN    std_logic_vector(11 DOWNTO 0);  -- sfix12
        dc_dig_q_in1                      :   IN    std_logic_vector(11 DOWNTO 0);  -- sfix12
        dc_i_out                          :   OUT   std_logic_vector(11 DOWNTO 0);  -- sfix12
        dc_q_out                          :   OUT   std_logic_vector(11 DOWNTO 0);  -- sfix12
        dc_gain_out                       :   OUT   std_logic_vector(11 DOWNTO 0);  -- sfix12_En6
        dc_blinky                         :   OUT   std_logic  -- ufix1
        );
END dc_offset;


ARCHITECTURE rtl OF dc_offset IS

  -- Signals
  SIGNAL dc_dig_i_in1_signed              : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL dc_dig_i_in1_1                   : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL gain_in_signed                   : signed(11 DOWNTO 0);  -- sfix12_En6
  SIGNAL gain_in_1                        : signed(11 DOWNTO 0);  -- sfix12_En6
  SIGNAL Product_out1                     : signed(23 DOWNTO 0);  -- sfix24_En6
  SIGNAL i_out                            : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL i_in                             : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL dc_dig_q_in1_signed              : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL dc_dig_q_in1_1                   : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL Product1_out1                    : signed(23 DOWNTO 0);  -- sfix24_En6
  SIGNAL q_out                            : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL q_in                             : signed(11 DOWNTO 0);  -- sfix12
  SIGNAL gain_out                         : signed(11 DOWNTO 0);  -- sfix12_En6
  SIGNAL HDL_Counter_out1                 : unsigned(25 DOWNTO 0);  -- ufix26
  SIGNAL HDL_Counter_step_reg             : unsigned(25 DOWNTO 0);  -- ufix26
  SIGNAL HDL_Counter_stepreg              : unsigned(25 DOWNTO 0);  -- ufix26
  SIGNAL HDL_Counter_count                : unsigned(25 DOWNTO 0);  -- ufix26
  SIGNAL Compare_To_Constant_out1         : std_logic;
  SIGNAL blinky                           : std_logic;  -- ufix1

BEGIN
  -- This version of the dc_offset block implements SW-controlled open-loop gain control only. DC offset, AGC, and RSSI have been removed at least for the time being.

  dc_dig_i_in1_signed <= signed(dc_dig_i_in1);

  in_1_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      dc_dig_i_in1_1 <= (OTHERS => '0');
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        dc_dig_i_in1_1 <= dc_dig_i_in1_signed;
      END IF;
    END IF;
  END PROCESS in_1_pipe_process;


  gain_in_signed <= signed(gain_in);

  in_0_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      gain_in_1 <= (OTHERS => '0');
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        gain_in_1 <= gain_in_signed;
      END IF;
    END IF;
  END PROCESS in_0_pipe_process;


  Product_out1 <= dc_dig_i_in1_1 * gain_in_1;

  i_out <= Product_out1(17 DOWNTO 6);

  Delay1_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      i_in <= (OTHERS => '0');
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        i_in <= i_out;
      END IF;
    END IF;
  END PROCESS Delay1_process;


  dc_i_out <= std_logic_vector(i_in);

  dc_dig_q_in1_signed <= signed(dc_dig_q_in1);

  in_2_pipe_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      dc_dig_q_in1_1 <= (OTHERS => '0');
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        dc_dig_q_in1_1 <= dc_dig_q_in1_signed;
      END IF;
    END IF;
  END PROCESS in_2_pipe_process;


  Product1_out1 <= dc_dig_q_in1_1 * gain_in_1;

  q_out <= Product1_out1(17 DOWNTO 6);

  Delay2_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      q_in <= (OTHERS => '0');
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        q_in <= q_out;
      END IF;
    END IF;
  END PROCESS Delay2_process;


  dc_q_out <= std_logic_vector(q_in);

  Delay7_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      gain_out <= (OTHERS => '0');
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        gain_out <= gain_in_1;
      END IF;
    END IF;
  END PROCESS Delay7_process;


  dc_gain_out <= std_logic_vector(gain_out);

  -- Count limited, Unsigned Counter
  --  initial value   = 0
  --  step value      = 1
  --  count to value  = 20000000
  HDL_Counter_step_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      HDL_Counter_step_reg <= (0 => '1', OTHERS => '0');
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        IF HDL_Counter_out1 = 19999999 THEN 
          HDL_Counter_step_reg <= (25 => '1', 23 DOWNTO 22 => '1', 19 DOWNTO 17 => '1', 15 DOWNTO 14 => '1', 12 => '1', 9 DOWNTO 8 => '1', OTHERS => '0');
        ELSE 
          HDL_Counter_step_reg <= (0 => '1', OTHERS => '0');
        END IF;
      END IF;
    END IF;
  END PROCESS HDL_Counter_step_process;

  HDL_Counter_stepreg <= HDL_Counter_step_reg;

  HDL_Counter_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      HDL_Counter_count <= (OTHERS => '0');
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        HDL_Counter_count <= HDL_Counter_count + HDL_Counter_stepreg;
      END IF;
    END IF;
  END PROCESS HDL_Counter_process;

  HDL_Counter_out1 <= HDL_Counter_count;

  
  Compare_To_Constant_out1 <= '1' WHEN HDL_Counter_out1 <= 10000000 ELSE
      '0';

  Delay6_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      blinky <= '0';
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        blinky <= Compare_To_Constant_out1;
      END IF;
    END IF;
  END PROCESS Delay6_process;



  dc_blinky <= blinky;

END rtl;

