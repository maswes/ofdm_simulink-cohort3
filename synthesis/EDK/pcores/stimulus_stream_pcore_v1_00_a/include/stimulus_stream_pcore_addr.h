/*
 * File Name:         hdl_prj\stimulus_stream\ipcore\stimulus_stream_pcore_v1_00_a\include\stimulus_stream_pcore_addr.h
 * Description:       C Header File
 * Created:           2015-05-10 23:11:29
*/

#ifndef STIMULUS_STREAM_PCORE_H_
#define STIMULUS_STREAM_PCORE_H_

#define  IPCore_Reset_stimulus_stream_pcore    0x0  //write 0x1 to bit 0 to reset IP core
#define  IPCore_Enable_stimulus_stream_pcore   0x4  //enabled (by default) when bit 0 is 0x1

#endif /* STIMULUS_STREAM_PCORE_H_ */
