-- -------------------------------------------------------------
-- 
-- File Name: C:\temp\simulink_code_gen\hdl_prj\hdlsrc\phy_layer59_delayed_sync\scramble_wrap.vhd
-- Created: 2015-06-07 04:17:10
-- 
-- Generated by MATLAB 8.3 and HDL Coder 3.4
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: scramble_wrap
-- Source Path: phy_layer59_delayed_sync/tx_upper/scramble_wrap
-- Hierarchy Level: 1
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY scramble_wrap IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        enb_1_2_0                         :   IN    std_logic;
        In1                               :   IN    std_logic;  -- ufix1
        In5                               :   IN    std_logic;  -- ufix1
        In4                               :   IN    std_logic;  -- ufix1
        In2                               :   IN    std_logic;  -- ufix1
        Out4                              :   OUT   std_logic;  -- ufix1
        Out2                              :   OUT   std_logic;  -- ufix1
        Out3                              :   OUT   std_logic  -- ufix1
        );
END scramble_wrap;


ARCHITECTURE rtl OF scramble_wrap IS

  -- Component Declarations
  COMPONENT Subsystem
    PORT( clk                             :   IN    std_logic;
          reset                           :   IN    std_logic;
          enb_1_2_0                       :   IN    std_logic;
          In2                             :   IN    std_logic;  -- ufix1
          In1                             :   IN    std_logic;  -- ufix1
          Enable                          :   IN    std_logic;
          Out2                            :   OUT   std_logic  -- ufix1
          );
  END COMPONENT;

  COMPONENT fwd_stream_pipe_block2
    PORT( clk                             :   IN    std_logic;
          reset                           :   IN    std_logic;
          enb_1_2_0                       :   IN    std_logic;
          In2                             :   IN    std_logic;  -- ufix1
          In1                             :   IN    std_logic;  -- ufix1
          In6                             :   IN    std_logic;  -- ufix1
          In5                             :   IN    std_logic;  -- ufix1
          In4                             :   IN    std_logic;  -- ufix1
          Out5                            :   OUT   std_logic;  -- ufix1
          Out1                            :   OUT   std_logic;  -- ufix1
          Out4                            :   OUT   std_logic;  -- ufix1
          Out2                            :   OUT   std_logic;  -- ufix1
          Out3                            :   OUT   std_logic  -- ufix1
          );
  END COMPONENT;

  -- Component Configuration Statements
  FOR ALL : Subsystem
    USE ENTITY work.Subsystem(rtl);

  FOR ALL : fwd_stream_pipe_block2
    USE ENTITY work.fwd_stream_pipe_block2(rtl);

  -- Signals
  SIGNAL Constant_out1                    : std_logic;  -- ufix1
  SIGNAL Constant1_out1                   : std_logic;  -- ufix1
  SIGNAL idata_ready                      : std_logic;  -- ufix1
  SIGNAL idata_ready_1                    : std_logic;  -- ufix1
  SIGNAL Logical_Operator2_out1           : std_logic;
  SIGNAL Logical_Operator1_out1           : std_logic;
  SIGNAL Delay_out1                       : std_logic;
  SIGNAL Delay_out1_1                     : std_logic;
  SIGNAL Subsystem_out1                   : std_logic;  -- ufix1
  SIGNAL odata                            : std_logic;  -- ufix1
  SIGNAL fwd_stream_pipe_odata_valid      : std_logic;  -- ufix1
  SIGNAL fwd_stream_pipe_idata_ready      : std_logic;  -- ufix1
  SIGNAL odata_valid                      : std_logic;  -- ufix1

BEGIN
  u_Subsystem : Subsystem
    PORT MAP( clk => clk,
              reset => reset,
              enb_1_2_0 => enb_1_2_0,
              In2 => In1,  -- ufix1
              In1 => In2,  -- ufix1
              Enable => Delay_out1_1,
              Out2 => Subsystem_out1  -- ufix1
              );

  u_fwd_stream_pipe : fwd_stream_pipe_block2
    PORT MAP( clk => clk,
              reset => reset,
              enb_1_2_0 => enb_1_2_0,
              In2 => Subsystem_out1,  -- ufix1
              In1 => Constant_out1,  -- ufix1
              In6 => Constant1_out1,  -- ufix1
              In5 => In5,  -- ufix1
              In4 => In4,  -- ufix1
              Out5 => odata,  -- ufix1
              Out1 => fwd_stream_pipe_odata_valid,  -- ufix1
              Out4 => fwd_stream_pipe_idata_ready,  -- ufix1
              Out2 => odata_valid,  -- ufix1
              Out3 => idata_ready  -- ufix1
              );

  Constant_out1 <= '0';

  Constant1_out1 <= '0';

  idata_ready_1 <=  NOT idata_ready;

  Logical_Operator2_out1 <= idata_ready_1 AND In5;

  Logical_Operator1_out1 <=  NOT Logical_Operator2_out1;

  Delay_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      Delay_out1 <= '0';
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        Delay_out1 <= Logical_Operator1_out1;
      END IF;
    END IF;
  END PROCESS Delay_process;


  Delay_out1_1 <= Delay_out1 AND In5;

  Out4 <= odata;

  Out2 <= odata_valid;

  Out3 <= idata_ready;

END rtl;

