-- -------------------------------------------------------------
-- 
-- File Name: C:\temp\simulink_code_gen\hdl_prj\hdlsrc\phy_layer59_delayed_sync\tx_upper.vhd
-- Created: 2015-06-07 04:17:10
-- 
-- Generated by MATLAB 8.3 and HDL Coder 3.4
-- 
-- 
-- -------------------------------------------------------------
-- Rate and Clocking Details
-- -------------------------------------------------------------
-- Model base rate: 0.5
-- Target subsystem base rate: 1
-- 
-- 
-- Clock Enable  Sample Time
-- -------------------------------------------------------------
-- ce_out        1
-- -------------------------------------------------------------
-- 
-- 
-- Output Signal                 Clock Enable  Sample Time
-- -------------------------------------------------------------
-- subc_map_odata_real           ce_out        1
-- subc_map_odata_imag           ce_out        1
-- subc_map_olast                ce_out        1
-- subc_map_ovalid               ce_out        1
-- test_scramble_odata           ce_out        1
-- test_scramble_ovalid          ce_out        1
-- test_scramble_oready          ce_out        1
-- test_conv_encode_odata_0      ce_out        1
-- test_conv_encode_odata_1      ce_out        1
-- test_conv_encode_odata_2      ce_out        1
-- test_conv_encode_odata_3      ce_out        1
-- test_conv_encode_ovalid       ce_out        1
-- test_conv_encode_oready       ce_out        1
-- test_serialize_odata          ce_out        1
-- test_serialize_ovalid         ce_out        1
-- test_serialize_oready         ce_out        1
-- test_qpsk_enc_odata_real      ce_out        1
-- test_qpek_enc_odata_imag      ce_out        1
-- test_qpsk_enc_ovalid          ce_out        1
-- test_qpsk_enc_oready          ce_out        1
-- test_tx_source_odata          ce_out        1
-- test_tx_source_ovalid         ce_out        1
-- test_tx_source_oready         ce_out        1
-- ifft_resetn_out               ce_out        1
-- -------------------------------------------------------------
-- 
-- -------------------------------------------------------------


-- -------------------------------------------------------------
-- 
-- Module: tx_upper
-- Source Path: phy_layer59_delayed_sync/tx_upper
-- Hierarchy Level: 0
-- 
-- -------------------------------------------------------------
LIBRARY IEEE;
USE IEEE.std_logic_1164.ALL;
USE IEEE.numeric_std.ALL;

ENTITY tx_upper IS
  PORT( clk                               :   IN    std_logic;
        reset                             :   IN    std_logic;
        clk_enable                        :   IN    std_logic;
        subc_map_oready                   :   IN    std_logic;  -- ufix1
        tx_idata                          :   IN    std_logic_vector(14 DOWNTO 0);  -- ufix15
        scrambler_bypass                  :   IN    std_logic;  -- ufix1
        conv_encode_bypass                :   IN    std_logic;  -- ufix1
        ce_out                            :   OUT   std_logic;
        subc_map_odata_real               :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En15
        subc_map_odata_imag               :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En15
        subc_map_olast                    :   OUT   std_logic;  -- ufix1
        subc_map_ovalid                   :   OUT   std_logic;  -- ufix1
        test_scramble_odata               :   OUT   std_logic;  -- ufix1
        test_scramble_ovalid              :   OUT   std_logic;  -- ufix1
        test_scramble_oready              :   OUT   std_logic;  -- ufix1
        test_conv_encode_odata_0          :   OUT   std_logic;  -- ufix1
        test_conv_encode_odata_1          :   OUT   std_logic;  -- ufix1
        test_conv_encode_odata_2          :   OUT   std_logic;  -- ufix1
        test_conv_encode_odata_3          :   OUT   std_logic;  -- ufix1
        test_conv_encode_ovalid           :   OUT   std_logic;  -- ufix1
        test_conv_encode_oready           :   OUT   std_logic;  -- ufix1
        test_serialize_odata              :   OUT   std_logic;  -- ufix1
        test_serialize_ovalid             :   OUT   std_logic;  -- ufix1
        test_serialize_oready             :   OUT   std_logic;  -- ufix1
        test_qpsk_enc_odata_real          :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En15
        test_qpek_enc_odata_imag          :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En15
        test_qpsk_enc_ovalid              :   OUT   std_logic;  -- ufix1
        test_qpsk_enc_oready              :   OUT   std_logic;  -- ufix1
        test_tx_source_odata              :   OUT   std_logic;  -- ufix1
        test_tx_source_ovalid             :   OUT   std_logic;  -- ufix1
        test_tx_source_oready             :   OUT   std_logic;  -- ufix1
        ifft_resetn_out                   :   OUT   std_logic  -- ufix1
        );
END tx_upper;


ARCHITECTURE rtl OF tx_upper IS

  -- Component Declarations
  COMPONENT tx_source
    PORT( clk                             :   IN    std_logic;
          reset                           :   IN    std_logic;
          enb_1_2_0                       :   IN    std_logic;
          tx_src_idata                    :   IN    std_logic_vector(14 DOWNTO 0);  -- ufix15
          tx_src_oready                   :   IN    std_logic;  -- ufix1
          tx_src_odata                    :   OUT   std_logic;  -- ufix1
          tx_src_ovalid                   :   OUT   std_logic  -- ufix1
          );
  END COMPONENT;

  COMPONENT scramble_wrap
    PORT( clk                             :   IN    std_logic;
          reset                           :   IN    std_logic;
          enb_1_2_0                       :   IN    std_logic;
          In1                             :   IN    std_logic;  -- ufix1
          In5                             :   IN    std_logic;  -- ufix1
          In4                             :   IN    std_logic;  -- ufix1
          In2                             :   IN    std_logic;  -- ufix1
          Out4                            :   OUT   std_logic;  -- ufix1
          Out2                            :   OUT   std_logic;  -- ufix1
          Out3                            :   OUT   std_logic  -- ufix1
          );
  END COMPONENT;

  COMPONENT conv_encoder_wrap
    PORT( clk                             :   IN    std_logic;
          reset                           :   IN    std_logic;
          enb_1_2_0                       :   IN    std_logic;
          In2                             :   IN    std_logic;  -- ufix1
          In5                             :   IN    std_logic;  -- ufix1
          In6                             :   IN    std_logic;  -- ufix1
          In1                             :   IN    std_logic;  -- ufix1
          Out4_0                          :   OUT   std_logic;  -- ufix1
          Out4_1                          :   OUT   std_logic;  -- ufix1
          Out4_2                          :   OUT   std_logic;  -- ufix1
          Out4_3                          :   OUT   std_logic;  -- ufix1
          Out5                            :   OUT   std_logic;  -- ufix1
          Out6                            :   OUT   std_logic  -- ufix1
          );
  END COMPONENT;

  COMPONENT new_serialize_wrap
    PORT( clk                             :   IN    std_logic;
          reset                           :   IN    std_logic;
          enb_1_2_0                       :   IN    std_logic;
          enb_const_rate                  :   IN    std_logic;
          In3_0                           :   IN    std_logic;  -- ufix1
          In3_1                           :   IN    std_logic;  -- ufix1
          In3_2                           :   IN    std_logic;  -- ufix1
          In3_3                           :   IN    std_logic;  -- ufix1
          In5                             :   IN    std_logic;  -- ufix1
          In4                             :   IN    std_logic;  -- ufix1
          Out4                            :   OUT   std_logic;  -- ufix1
          Out3                            :   OUT   std_logic;  -- ufix1
          Out5                            :   OUT   std_logic  -- ufix1
          );
  END COMPONENT;

  COMPONENT qpsk_encode_wrap
    PORT( clk                             :   IN    std_logic;
          reset                           :   IN    std_logic;
          enb_1_2_0                       :   IN    std_logic;
          enb_const_rate                  :   IN    std_logic;
          In3                             :   IN    std_logic;  -- ufix1
          In5                             :   IN    std_logic;  -- ufix1
          In4                             :   IN    std_logic;  -- ufix1
          Out4_re                         :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En15
          Out4_im                         :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En15
          Out3                            :   OUT   std_logic;  -- ufix1
          Out5                            :   OUT   std_logic  -- ufix1
          );
  END COMPONENT;

  COMPONENT subcarrier_map_synth_friendly
    PORT( clk                             :   IN    std_logic;
          reset                           :   IN    std_logic;
          enb_1_2_0                       :   IN    std_logic;
          In6                             :   IN    std_logic;  -- ufix1
          In1_re                          :   IN    std_logic_vector(15 DOWNTO 0);  -- sfix16_En15
          In1_im                          :   IN    std_logic_vector(15 DOWNTO 0);  -- sfix16_En15
          idata_valid                     :   IN    std_logic;  -- ufix1
          Out6_re                         :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En15
          Out6_im                         :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En15
          Out8                            :   OUT   std_logic;  -- ufix1
          Out7                            :   OUT   std_logic;  -- ufix1
          Out1                            :   OUT   std_logic;  -- ufix1
          Out9                            :   OUT   std_logic  -- ufix1
          );
  END COMPONENT;

  COMPONENT tx_fwd_stream_pipe
    PORT( clk                             :   IN    std_logic;
          reset                           :   IN    std_logic;
          enb_1_2_0                       :   IN    std_logic;
          In2                             :   IN    std_logic_vector(15 DOWNTO 0);  -- sfix16_En15
          In1                             :   IN    std_logic_vector(15 DOWNTO 0);  -- sfix16_En15
          In6                             :   IN    std_logic;  -- ufix1
          In5                             :   IN    std_logic;  -- ufix1
          In4                             :   IN    std_logic;  -- ufix1
          Out5                            :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En15
          Out1                            :   OUT   std_logic_vector(15 DOWNTO 0);  -- sfix16_En15
          Out4                            :   OUT   std_logic;  -- ufix1
          Out2                            :   OUT   std_logic;  -- ufix1
          Out3                            :   OUT   std_logic  -- ufix1
          );
  END COMPONENT;

  -- Component Configuration Statements
  FOR ALL : tx_source
    USE ENTITY work.tx_source(rtl);

  FOR ALL : scramble_wrap
    USE ENTITY work.scramble_wrap(rtl);

  FOR ALL : conv_encoder_wrap
    USE ENTITY work.conv_encoder_wrap(rtl);

  FOR ALL : new_serialize_wrap
    USE ENTITY work.new_serialize_wrap(rtl);

  FOR ALL : qpsk_encode_wrap
    USE ENTITY work.qpsk_encode_wrap(rtl);

  FOR ALL : subcarrier_map_synth_friendly
    USE ENTITY work.subcarrier_map_synth_friendly(rtl);

  FOR ALL : tx_fwd_stream_pipe
    USE ENTITY work.tx_fwd_stream_pipe(rtl);

  -- Signals
  SIGNAL enb_1_2_0                        : std_logic;
  SIGNAL odata_im                         : std_logic_vector(15 DOWNTO 0);  -- ufix16
  SIGNAL subc_map_odata_imag_1            : signed(15 DOWNTO 0);  -- sfix16_En15
  SIGNAL idata_ready                      : std_logic;  -- ufix1
  SIGNAL test_tx_source_odata_1           : std_logic;  -- ufix1
  SIGNAL test_tx_source_ovalid_1          : std_logic;  -- ufix1
  SIGNAL idata_ready_1                    : std_logic;  -- ufix1
  SIGNAL test_scramble_odata_1            : std_logic;  -- ufix1
  SIGNAL odata_valid                      : std_logic;  -- ufix1
  SIGNAL iready                           : std_logic;  -- ufix1
  SIGNAL test_conv_encode_odata_0_1       : std_logic;  -- ufix1
  SIGNAL test_conv_encode_odata_1_1       : std_logic;  -- ufix1
  SIGNAL test_conv_encode_odata_2_1       : std_logic;  -- ufix1
  SIGNAL test_conv_encode_odata_3_1       : std_logic;  -- ufix1
  SIGNAL odata_valid_1                    : std_logic;  -- ufix1
  SIGNAL test_serialize_oready_1          : std_logic;  -- ufix1
  SIGNAL test_serialize_odata_1           : std_logic;  -- ufix1
  SIGNAL odata_valid_2                    : std_logic;  -- ufix1
  SIGNAL idata_ready_2                    : std_logic;  -- ufix1
  SIGNAL test_qpsk_odata_re               : std_logic_vector(15 DOWNTO 0);  -- ufix16
  SIGNAL test_qpsk_odata_im               : std_logic_vector(15 DOWNTO 0);  -- ufix16
  SIGNAL test_qpsk_enc_ovalid_1           : std_logic;  -- ufix1
  SIGNAL idata_ready_3                    : std_logic;  -- ufix1
  SIGNAL odata_re                         : std_logic_vector(15 DOWNTO 0);  -- ufix16
  SIGNAL odata_last                       : std_logic;  -- ufix1
  SIGNAL odata_valid_3                    : std_logic;  -- ufix1
  SIGNAL pilot_det_en                     : std_logic;  -- ufix1
  SIGNAL subc_map_odata_real_1            : signed(15 DOWNTO 0);  -- sfix16_En15
  SIGNAL subc_map_odata_real_tmp          : std_logic_vector(15 DOWNTO 0);  -- ufix16
  SIGNAL subc_map_odata_imag_tmp          : std_logic_vector(15 DOWNTO 0);  -- ufix16
  SIGNAL odata_valid_4                    : std_logic;  -- ufix1
  SIGNAL test_qpsk_enc_odata_real_tmp     : signed(15 DOWNTO 0);  -- sfix16_En15
  SIGNAL test_qpek_enc_odata_imag_tmp     : signed(15 DOWNTO 0);  -- sfix16_En15
  SIGNAL Constant_out1                    : std_logic;  -- ufix1
  SIGNAL Delay_reg                        : std_logic_vector(0 TO 1);  -- ufix1 [2]

BEGIN
  u_tx_source : tx_source
    PORT MAP( clk => clk,
              reset => reset,
              enb_1_2_0 => clk_enable,
              tx_src_idata => tx_idata,  -- ufix15
              tx_src_oready => idata_ready,  -- ufix1
              tx_src_odata => test_tx_source_odata_1,  -- ufix1
              tx_src_ovalid => test_tx_source_ovalid_1  -- ufix1
              );

  u_scramble_wrap : scramble_wrap
    PORT MAP( clk => clk,
              reset => reset,
              enb_1_2_0 => clk_enable,
              In1 => test_tx_source_odata_1,  -- ufix1
              In5 => test_tx_source_ovalid_1,  -- ufix1
              In4 => idata_ready_1,  -- ufix1
              In2 => scrambler_bypass,  -- ufix1
              Out4 => test_scramble_odata_1,  -- ufix1
              Out2 => odata_valid,  -- ufix1
              Out3 => idata_ready  -- ufix1
              );

  u_conv_encoder_wrap : conv_encoder_wrap
    PORT MAP( clk => clk,
              reset => reset,
              enb_1_2_0 => clk_enable,
              In2 => test_scramble_odata_1,  -- ufix1
              In5 => odata_valid,  -- ufix1
              In6 => iready,  -- ufix1
              In1 => conv_encode_bypass,  -- ufix1
              Out4_0 => test_conv_encode_odata_0_1,  -- ufix1
              Out4_1 => test_conv_encode_odata_1_1,  -- ufix1
              Out4_2 => test_conv_encode_odata_2_1,  -- ufix1
              Out4_3 => test_conv_encode_odata_3_1,  -- ufix1
              Out5 => odata_valid_1,  -- ufix1
              Out6 => idata_ready_1  -- ufix1
              );

  u_new_serialize_wrap : new_serialize_wrap
    PORT MAP( clk => clk,
              reset => reset,
              enb_1_2_0 => clk_enable,
              enb_const_rate => clk_enable,
              In3_0 => test_conv_encode_odata_0_1,  -- ufix1
              In3_1 => test_conv_encode_odata_1_1,  -- ufix1
              In3_2 => test_conv_encode_odata_2_1,  -- ufix1
              In3_3 => test_conv_encode_odata_3_1,  -- ufix1
              In5 => odata_valid_1,  -- ufix1
              In4 => test_serialize_oready_1,  -- ufix1
              Out4 => test_serialize_odata_1,  -- ufix1
              Out3 => odata_valid_2,  -- ufix1
              Out5 => iready  -- ufix1
              );

  u_qpsk_encode_wrap : qpsk_encode_wrap
    PORT MAP( clk => clk,
              reset => reset,
              enb_1_2_0 => clk_enable,
              enb_const_rate => clk_enable,
              In3 => test_serialize_odata_1,  -- ufix1
              In5 => odata_valid_2,  -- ufix1
              In4 => idata_ready_2,  -- ufix1
              Out4_re => test_qpsk_odata_re,  -- sfix16_En15
              Out4_im => test_qpsk_odata_im,  -- sfix16_En15
              Out3 => test_qpsk_enc_ovalid_1,  -- ufix1
              Out5 => test_serialize_oready_1  -- ufix1
              );

  u_subcarrier_map_synth_friendly : subcarrier_map_synth_friendly
    PORT MAP( clk => clk,
              reset => reset,
              enb_1_2_0 => clk_enable,
              In6 => idata_ready_3,  -- ufix1
              In1_re => test_qpsk_odata_re,  -- sfix16_En15
              In1_im => test_qpsk_odata_im,  -- sfix16_En15
              idata_valid => test_qpsk_enc_ovalid_1,  -- ufix1
              Out6_re => odata_re,  -- sfix16_En15
              Out6_im => odata_im,  -- sfix16_En15
              Out8 => odata_last,  -- ufix1
              Out7 => odata_valid_3,  -- ufix1
              Out1 => idata_ready_2,  -- ufix1
              Out9 => pilot_det_en  -- ufix1
              );

  u_tx_fwd_stream_pipe : tx_fwd_stream_pipe
    PORT MAP( clk => clk,
              reset => reset,
              enb_1_2_0 => clk_enable,
              In2 => std_logic_vector(subc_map_odata_real_1),  -- sfix16_En15
              In1 => std_logic_vector(subc_map_odata_imag_1),  -- sfix16_En15
              In6 => odata_last,  -- ufix1
              In5 => odata_valid_3,  -- ufix1
              In4 => subc_map_oready,  -- ufix1
              Out5 => subc_map_odata_real_tmp,  -- sfix16_En15
              Out1 => subc_map_odata_imag_tmp,  -- sfix16_En15
              Out4 => subc_map_olast,  -- ufix1
              Out2 => odata_valid_4,  -- ufix1
              Out3 => idata_ready_3  -- ufix1
              );

  subc_map_odata_imag_1 <= signed(odata_im);

  subc_map_odata_real_1 <= signed(odata_re);

  test_conv_encode_odata_0 <= test_conv_encode_odata_0_1;

  test_conv_encode_odata_1 <= test_conv_encode_odata_1_1;

  test_conv_encode_odata_2 <= test_conv_encode_odata_2_1;

  test_conv_encode_odata_3 <= test_conv_encode_odata_3_1;

  test_qpsk_enc_odata_real_tmp <= signed(test_qpsk_odata_re);

  test_qpsk_enc_odata_real <= std_logic_vector(test_qpsk_enc_odata_real_tmp);

  test_qpek_enc_odata_imag_tmp <= signed(test_qpsk_odata_im);

  test_qpek_enc_odata_imag <= std_logic_vector(test_qpek_enc_odata_imag_tmp);

  Constant_out1 <= '1';

  enb_1_2_0 <= clk_enable;

  Delay_process : PROCESS (clk, reset)
  BEGIN
    IF reset = '1' THEN
      Delay_reg <= (OTHERS => '0');
    ELSIF clk'EVENT AND clk = '1' THEN
      IF enb_1_2_0 = '1' THEN
        Delay_reg(0) <= Constant_out1;
        Delay_reg(1) <= Delay_reg(0);
      END IF;
    END IF;
  END PROCESS Delay_process;

  ifft_resetn_out <= Delay_reg(1);

  ce_out <= clk_enable;





  subc_map_odata_real <= subc_map_odata_real_tmp;

  subc_map_odata_imag <= subc_map_odata_imag_tmp;

  subc_map_ovalid <= odata_valid_4;

  test_scramble_odata <= test_scramble_odata_1;

  test_scramble_ovalid <= odata_valid;

  test_scramble_oready <= idata_ready_1;

  test_conv_encode_ovalid <= odata_valid_1;

  test_conv_encode_oready <= iready;

  test_serialize_odata <= test_serialize_odata_1;

  test_serialize_ovalid <= odata_valid_2;

  test_serialize_oready <= test_serialize_oready_1;

  test_qpsk_enc_ovalid <= test_qpsk_enc_ovalid_1;

  test_qpsk_enc_oready <= idata_ready_2;

  test_tx_source_odata <= test_tx_source_odata_1;

  test_tx_source_ovalid <= test_tx_source_ovalid_1;

  test_tx_source_oready <= idata_ready;

END rtl;

