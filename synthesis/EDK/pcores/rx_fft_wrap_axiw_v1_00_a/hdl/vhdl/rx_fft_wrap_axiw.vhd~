-------------------------------------------------------------------
-- System Generator version 13.1.00 VHDL source file.
--
-- Copyright(C) 2011 by Xilinx, Inc.  All rights reserved.  This
-- text/file contains proprietary, confidential information of Xilinx,
-- Inc., is distributed under license from Xilinx, Inc., and may be used,
-- copied and/or disclosed only pursuant to the terms of a valid license
-- agreement with Xilinx, Inc.  Xilinx hereby grants you a license to use
-- this text/file solely for design, simulation, implementation and
-- creation of design files limited to Xilinx devices or technologies.
-- Use with non-Xilinx devices or technologies is expressly prohibited
-- and immediately terminates your license unless covered by a separate
-- agreement.
--
-- Xilinx is providing this design, code, or information "as is" solely
-- for use in developing programs and solutions for Xilinx devices.  By
-- providing this design, code, or information as one possible
-- implementation of this feature, application or standard, Xilinx is
-- making no representation that this implementation is free from any
-- claims of infringement.  You are responsible for obtaining any rights
-- you may require for your implementation.  Xilinx expressly disclaims
-- any warranty whatsoever with respect to the adequacy of the
-- implementation, including but not limited to warranties of
-- merchantability or fitness for a particular purpose.
--
-- Xilinx products are not intended for use in life support appliances,
-- devices, or systems.  Use in such applications is expressly prohibited.
--
-- Any modifications that are made to the source code are done at the user's
-- sole risk and will be unsupported.
--
-- This copyright and support notice must be retained as part of this
-- text at all times.  
-- (c) Copyright 1995-2011 Xilinx, Inc.  All rightsreserved.
-------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;

entity axiaddrpref is
    generic (
        C_BASEADDR : std_logic_vector(31 downto 0) := X"80000000";
        C_HIGHADDR : std_logic_vector(31 downto 0) := X"8000FFFF";
        C_S_AXI_ID_WIDTH: integer := 1;
        C_S_AXI_NATIVE_ID_WIDTH: integer := 8
    );
    port (
        -- arid
        sg_s_axi_arid: in std_logic_vector(C_S_AXI_ID_WIDTH-1 downto 0);
        s_axi_arid: out std_logic_vector(C_S_AXI_NATIVE_ID_WIDTH-1 downto 0);
        -- awid
        sg_s_axi_awid: in std_logic_vector(C_S_AXI_ID_WIDTH-1 downto 0);
        s_axi_awid: out std_logic_vector(C_S_AXI_NATIVE_ID_WIDTH-1 downto 0);
        -- rid
        sg_s_axi_rid: out std_logic_vector(C_S_AXI_ID_WIDTH-1 downto 0);
        s_axi_rid: in std_logic_vector(C_S_AXI_NATIVE_ID_WIDTH-1 downto 0);
        -- bid
        sg_s_axi_bid: out std_logic_vector(C_S_AXI_ID_WIDTH-1 downto 0);
        s_axi_bid: in std_logic_vector(C_S_AXI_NATIVE_ID_WIDTH-1 downto 0)
    );
end axiaddrpref;

architecture behavior of axiaddrpref is

begin

axiaddrpref_less: if (C_S_AXI_ID_WIDTH <= C_S_AXI_NATIVE_ID_WIDTH) generate
  s_axi_arid(C_S_AXI_ID_WIDTH-1 downto 0) <= sg_s_axi_arid;
  s_axi_awid(C_S_AXI_ID_WIDTH-1 downto 0) <= sg_s_axi_awid;

  sg_s_axi_rid <= s_axi_rid(C_S_AXI_ID_WIDTH-1 downto 0);
  sg_s_axi_bid <= s_axi_bid(C_S_AXI_ID_WIDTH-1 downto 0);
end generate axiaddrpref_less;

axiaddrpref_greater: if (C_S_AXI_ID_WIDTH > C_S_AXI_NATIVE_ID_WIDTH) generate
  s_axi_arid <= sg_s_axi_arid(C_S_AXI_NATIVE_ID_WIDTH-1 downto 0);
  s_axi_awid <= sg_s_axi_awid(C_S_AXI_NATIVE_ID_WIDTH-1 downto 0);

  sg_s_axi_rid(C_S_AXI_NATIVE_ID_WIDTH-1 downto 0) <= s_axi_rid;
  sg_s_axi_bid(C_S_AXI_NATIVE_ID_WIDTH-1 downto 0) <= s_axi_bid;

-- Set upper 4 bits to 1000 for general performance, 0000 for high performance
  sg_s_axi_rid(C_S_AXI_ID_WIDTH - 1 downto C_S_AXI_ID_WIDTH - 4) <= "1000";
  sg_s_axi_bid(C_S_AXI_ID_WIDTH - 1 downto C_S_AXI_ID_WIDTH - 4) <= "1000";

end generate axiaddrpref_greater;

end behavior;

library IEEE;
use IEEE.std_logic_1164.all;
use work.conv_pkg.all;

entity rx_fft_wrap_axiw is
  generic (
    C_BASEADDR: std_logic_vector(31 downto 0) := X"80000000";
    C_HIGHADDR: std_logic_vector(31 downto 0) := X"80000FFF";
    C_S_AXI_ADDR_WIDTH: integer := 0;
    C_S_AXI_DATA_WIDTH: integer := 0;
    C_S_AXI_ID_WIDTH: integer := 0;
    C_S_AXI_SUPPORT_BURST: integer := 0
  );
  port (
    fft_aresetn: in std_logic; 
    fft_idata_imag1: in std_logic_vector(0 to 15); 
    fft_idata_last1: in std_logic; 
    fft_idata_real1: in std_logic_vector(0 to 15); 
    fft_idata_valid1: in std_logic; 
    fft_odata_ready1: in std_logic; 
    sysgen_clk: in std_logic; 
    fft_cfg_ready: out std_logic; 
    fft_idata_ready: out std_logic; 
    fft_odata_imag: out std_logic_vector(0 to 15); 
    fft_odata_last: out std_logic; 
    fft_odata_real: out std_logic_vector(0 to 15); 
    fft_odata_valid: out std_logic; 
    fft_status: out std_logic_vector(0 to 5)
  );
end rx_fft_wrap_axiw;

architecture structural of rx_fft_wrap_axiw is
  signal clk: std_logic;
  signal fft_aresetn_x0: std_logic;
  signal fft_cfg_ready_x0: std_logic;
  signal fft_idata_imag1_x0: std_logic_vector(15 downto 0);
  signal fft_idata_last1_x0: std_logic;
  signal fft_idata_ready_x0: std_logic;
  signal fft_idata_real1_x0: std_logic_vector(15 downto 0);
  signal fft_idata_valid1_x0: std_logic;
  signal fft_odata_imag_x0: std_logic_vector(15 downto 0);
  signal fft_odata_last_x0: std_logic;
  signal fft_odata_ready1_x0: std_logic;
  signal fft_odata_real_x0: std_logic_vector(15 downto 0);
  signal fft_odata_valid_x0: std_logic;
  signal fft_status_x0: std_logic_vector(5 downto 0);

begin
  fft_aresetn_x0 <= fft_aresetn;
  fft_idata_imag1_x0 <= fft_idata_imag1;
  fft_idata_last1_x0 <= fft_idata_last1;
  fft_idata_real1_x0 <= fft_idata_real1;
  fft_idata_valid1_x0 <= fft_idata_valid1;
  fft_odata_ready1_x0 <= fft_odata_ready1;
  clk <= sysgen_clk;
  fft_cfg_ready <= fft_cfg_ready_x0;
  fft_idata_ready <= fft_idata_ready_x0;
  fft_odata_imag <= fft_odata_imag_x0;
  fft_odata_last <= fft_odata_last_x0;
  fft_odata_real <= fft_odata_real_x0;
  fft_odata_valid <= fft_odata_valid_x0;
  fft_status <= fft_status_x0;

  axiaddrpref_x0: entity work.axiaddrpref
    generic map (
      C_BASEADDR => C_BASEADDR,
      C_HIGHADDR => C_HIGHADDR,
      C_S_AXI_ID_WIDTH => C_S_AXI_ID_WIDTH
    );

  sysgen_dut: entity work.rx_fft_wrap_cw
    port map (
      clk => clk,
      fft_aresetn => fft_aresetn_x0,
      fft_idata_imag1 => fft_idata_imag1_x0,
      fft_idata_last1 => fft_idata_last1_x0,
      fft_idata_real1 => fft_idata_real1_x0,
      fft_idata_valid1 => fft_idata_valid1_x0,
      fft_odata_ready1 => fft_odata_ready1_x0,
      fft_cfg_ready => fft_cfg_ready_x0,
      fft_idata_ready => fft_idata_ready_x0,
      fft_odata_imag => fft_odata_imag_x0,
      fft_odata_last => fft_odata_last_x0,
      fft_odata_real => fft_odata_real_x0,
      fft_odata_valid => fft_odata_valid_x0,
      fft_status => fft_status_x0
    );

end structural;
