
/*******************************************************************
*
* CAUTION: This file is automatically generated by libgen.
* Version: Xilinx EDK 14.7 EDK_P.20131013
* DO NOT EDIT.
*
* Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.

* 
* Description: Driver configuration
*
*******************************************************************/

#include "xparameters.h"
#include "xuartlite.h"

/*
* The configuration table for devices
*/

XUartLite_Config XUartLite_ConfigTable[] =
{
	{
		XPAR_MCU_UART_DEVICE_ID,
		XPAR_MCU_UART_BASEADDR,
		XPAR_MCU_UART_BAUDRATE,
		XPAR_MCU_UART_USE_PARITY,
		XPAR_MCU_UART_ODD_PARITY,
		XPAR_MCU_UART_DATA_BITS
	}
};


