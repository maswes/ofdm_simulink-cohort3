
/*******************************************************************
*
* CAUTION: This file is automatically generated by libgen.
* Version: Xilinx EDK 14.7 EDK_P.20131013
* DO NOT EDIT.
*
* Copyright (c) 1995-2012 Xilinx, Inc.  All rights reserved.

* 
* Description: Driver configuration
*
*******************************************************************/

#include "xparameters.h"
#include "xgpio.h"

/*
* The configuration table for devices
*/

XGpio_Config XGpio_ConfigTable[] =
{
	{
		XPAR_AXI_GPIO_BUTTON_DEVICE_ID,
		XPAR_AXI_GPIO_BUTTON_BASEADDR,
		XPAR_AXI_GPIO_BUTTON_INTERRUPT_PRESENT,
		XPAR_AXI_GPIO_BUTTON_IS_DUAL
	},
	{
		XPAR_AXI_GPIO_LED_DEVICE_ID,
		XPAR_AXI_GPIO_LED_BASEADDR,
		XPAR_AXI_GPIO_LED_INTERRUPT_PRESENT,
		XPAR_AXI_GPIO_LED_IS_DUAL
	},
	{
		XPAR_AXI_GPIO_SWITCH_DEVICE_ID,
		XPAR_AXI_GPIO_SWITCH_BASEADDR,
		XPAR_AXI_GPIO_SWITCH_INTERRUPT_PRESENT,
		XPAR_AXI_GPIO_SWITCH_IS_DUAL
	}
};


