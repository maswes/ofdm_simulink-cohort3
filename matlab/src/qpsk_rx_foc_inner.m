%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% QPSK demonstration packet-based transceiver for Chilipepper
% Toyon Research Corp.
% http://www.toyon.com/chilipepper.php
% Created 10/17/2012
% Modified by G. Woods, UCSD
% embedded@toyon.com
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Demonstration of a Costas Loop. Refer to:
% Telecommunications Breakdown: Concepts of Communication Transmitted via 
% Software-Defined Radio C. Richard Johnson
% We employ a hard-decision feedback in order to get rid of the loop
% filters.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%#codegen
function [z_i_out, z_q_out, fe] = qpsk_rx_foc(y_i, y_q, bypass, est_enable)

persistent phi

lSin = SIN;
lCos = COS;

 
if isempty(phi)
    phi = fi(0, 1,26,12);
end

% create the VCO signal
%phi = mod(phi,1);
if phi >= 1
    phi = fi(phi - 1, 1,26,12);
end
if phi < 0
    phi = fi(phi + 1, 1,26,12);
end

phi12 = fi(round(phi*2^12)+1, 0,13,0);
if phi12 >= 2^12
    phi12 = fi(1, 0,13,0);
end
if phi12 < 0
    phi12 = fi(0, 0,13,0);
end
f_i = fi(lCos(phi12+1), 1, 26, 12);
f_q = fi(lSin(phi12+1), 1, 26, 12);
ti1 = fi(y_i*f_i, 1, 26, 12);
ti2 = fi(y_q*f_q, 1, 26, 12);
tq1 = fi(y_q*f_i, 1, 26, 12);
tq2 = fi(-y_i*f_q, 1, 26, 12);
z_i = fi(ti1 + ti2, 1, 26, 12);
z_q = fi(tq1 + tq2, 1, 26, 12);

% generate the error term to drive VCO generateion
if z_q < 0
    tf = fi(-z_i, 1,26,12);
else
    tf = fi(z_i, 1,26,12);
end
if z_i < 0
    bf = fi(-z_q, 1,26,12);
else
    bf = fi(z_q, 1,26,12);
end
% using sign of error in order to make it gain invariant
time_diff = tf-bf;
if time_diff < 0
    e = -1;
else
    e = 1;
end

% Edit to support periodic preamble (g. woods)
if est_enable == 1
    % update with hard coded mu 40/2^12
    %c = (0.009765625)*e;   % Original value
    c = 0.3*(0.009765625)*e; % Value for finer-grain inner loop
else
    c = 0;
end

phiNew = fi(phi - c, 1,26,12);
phi = fi(phiNew, 1,26,12)

fe = fi(phiNew, 1,26,12);

%z_i_out = z_i;
%z_q_out = z_q;

z_i_out = fi(z_i, 1, 26, 12);
z_q_out = fi(z_q, 1, 26, 12);

%z_i_out = fi(y_i, 1, 26, 12);
%z_q_out = fi(y_q, 1, 26, 12);
%fe      = fi(0, 1, 26, 12);