function [ odata ] = qpsk_lut( idata )
% Function:     qpsk_lut()
% Description:  Binary to fixed-point QPSK component value conversion
% Author:       Greg Woods

switch idata
    case 1
        odata = fi(0.7071, 1, 16, 15);
    case 0
        odata = fi(-0.7071, 1, 16, 15);
    otherwise
        odata = fi(0, 1, 16, 15);   % Illegal condition that shouldn't happen
end


end

