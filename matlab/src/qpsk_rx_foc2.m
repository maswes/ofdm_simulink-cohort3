%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% QPSK demonstration packet-based transceiver for Chilipepper
% Toyon Research Corp.
% http://www.toyon.com/chilipepper.php
% Created 10/17/2012
% embedded@toyon.com
% Modified by G. Woods, UCSD
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Demonstration of a Costas Loop. Refer to:
% Telecommunications Breakdown: Concepts of Communication Transmitted via 
% Software-Defined Radio C. Richard Johnson
% We employ a hard-decision feedback in order to get rid of the loop
% filters.
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%#codegen
function [z_i_out, z_q_out, chan2_z_i_out, chan2_z_q_out, fe, freq_out] ...
    = qpsk_rx_foc(y_i, y_q, chan2_y_i, chan2_y_q, est_enable, track_en)

persistent phi
persistent freq_mean
persistent next_freq_mean
persistent next_freq_in

lSin = SIN;
lCos = COS;

ONE          = fi( 1,1,2,0);      % Fixed-point 1 constant
ZERO         = fi( 0,1,2,0);     % Fixed-point 0 constant
NEGATIVE_ONE = fi(-1,1,2,0);      % Fixed-point -1 constant

if isempty(phi)
    %phi = fi(0, 1,26,12);
    phi = fi(0, 1,26,22);
end
alpha_type = fi(0, 0, 16,15);

if isempty(freq_mean)
    freq_in = fi(0,1,24,22);
    next_freq_in = fi(0,1,24,22);
    freq_mean = fi(0,1,24,22);
    next_freq_mean = fi(0,1,24,22);
    freq_out = fi(0,1,24,22);
end

freq_mean = next_freq_mean;
freq_in   = next_freq_in;


% create the VCO signal
%phi = mod(phi,1);
if phi >= 1
    %phi = fi(phi - 1, 1,26,12);
    phi = fi(phi - ONE, 1,26,22);
end
if phi < 0
    %phi = fi(phi + 1, 1,26,12);
    phi = fi(phi + ONE, 1,26,22);
end

phi12 = fi(round(phi*2^12)+1, 0,13,0);
%phi12 = fi(round(phi*2^12)+1, 0,26,22);
%phi12 = cast(round(phi*2^12)+1, 'like', phi12);
if phi12 >= 2^12
    %phi12 = fi(1, 0,13,0);
    phi12 = cast(1, 'like', phi12);
end
if phi12 < 0
    %phi12 = fi(0, 0,13,0);
    phi12 = cast(0, 'like', phi12);
end
%f_i = fi(lCos(phi12+1), 1, 26, 12);
%f_q = fi(lSin(phi12+1), 1, 26, 12);
f_i = fi(lCos(phi12+ONE), 1, 26, 22);
f_q = fi(lSin(phi12+ONE), 1, 26, 22);

% ti1 = fi(y_i*f_i, 1, 26, 12);
% ti2 = fi(y_q*f_q, 1, 26, 12);
% tq1 = fi(y_q*f_i, 1, 26, 12);
% tq2 = fi(-y_i*f_q, 1, 26, 12);
% z_i = fi(ti1 + ti2, 1, 26, 12);
% z_q = fi(tq1 + tq2, 1, 26, 12);

ti1 = fi(y_i*f_i, 1, 26, 22);
ti2 = fi(y_q*f_q, 1, 26, 22);
tq1 = fi(y_q*f_i, 1, 26, 22);
tq2 = fi(-y_i*f_q, 1, 26, 22);
z_i = fi(ti1 + ti2, 1, 26, 22);
z_q = fi(tq1 + tq2, 1, 26, 22);

% chan2_ti1 = fi(chan2_y_i*f_i, 1, 26, 12);
% chan2_ti2 = fi(chan2_y_q*f_q, 1, 26, 12);
% chan2_tq1 = fi(chan2_y_q*f_i, 1, 26, 12);
% chan2_tq2 = fi(-chan2_y_i*f_q, 1, 26, 12);
% chan2_z_i = fi(chan2_ti1 + chan2_ti2, 1, 26, 12);
% chan2_z_q = fi(chan2_tq1 + chan2_tq2, 1, 26, 12);
chan2_ti1 = fi(chan2_y_i*f_i, 1, 26, 22);
chan2_ti2 = fi(chan2_y_q*f_q, 1, 26, 22);
chan2_tq1 = fi(chan2_y_q*f_i, 1, 26, 22);
chan2_tq2 = fi(-chan2_y_i*f_q, 1, 26, 22);
chan2_z_i = fi(chan2_ti1 + chan2_ti2, 1, 26, 22);
chan2_z_q = fi(chan2_tq1 + chan2_tq2, 1, 26, 22);

% generate the error term to drive VCO generateion
if z_q < 0
%    tf = fi(-z_i, 1,26,12);
    tf = fi(-z_i, 1,26,22);
else
%    tf = fi(z_i, 1,26,12);
    tf = fi(z_i, 1,26,22);
end
if z_i < 0
%    bf = fi(-z_q, 1,26,12);
    bf = fi(-z_q, 1,26,22);
else
%    bf = fi(z_q, 1,26,12);
    bf = fi(z_q, 1,26,22);
end
% using sign of error in order to make it gain invariant
time_diff = tf-bf;
if time_diff < 0
    %e = -1;
    e = NEGATIVE_ONE;
%else
%    e = 1;
elseif time_diff > 0    % gwoods edit
    %e = 1;
    e = ONE;
else
    %e = 0;              % gwoods edit
    e = ZERO;
end

% % Edit to support periodic preamble (g. woods)
% if est_enable == 1
%     % update with hard coded mu 40/2^12
%     %c = (0.009765625)*e;   % Original value
%     c = 0.3*(0.009765625)*e; % Value for finer-grain inner loop
% else
%     c = 0;
% end

% Edit to support periodic preamble (g. woods)
c = fi(0, 1, 26,24);
if est_enable == 1  %Course freq acquisition
    % update with hard coded mu 40/2^12
    %c = (0.009765625)*e;
%    c = cast((0.009765625)*e, 'like', c);
%    c = cast((0.009765625)*e/8, 'like', c);  % Experiment -gw
%    c = cast((0.009765625)*e*(2/8), 'like', c);  % Experiment -gw

%    c = cast((0.009765625)*e*(2/8), 'like', c);  % Experiment -gw
    c = cast((0.009765625)*e*(2/8), 'like', c);  % Experiment -gw
elseif track_en == 1 %Fine freq tracking
    % update with hard coded mu 40/2^12
    %c = (0.009765625)*e;
%    c = cast((0.009765625)*e, 'like', c);
%    c = cast((0.009765625)*e/8, 'like', c);  % Experiment -gw
%    c = cast((0.009765625)*e, 'like', c);  % Experiment -gw
    c = cast((0.009765625)*e, 'like', c);  % Experiment -gw
else
    c = cast(-freq_mean, 'like', c);
end

%phiOld = fi(phi, 1,26,12);
%phiNew = fi(phi - c, 1,26,12)
%phi = fi(phiNew, 1,26,12);
%fe = fi(phiNew, 1,26,12);
phiOld = fi(phi, 1,26,22);
phiNew = fi(phi - c, 1,26,22);
phi = fi(phiNew, 1,26,22);
fe = fi(phiNew, 1,26,22);

%z_i_out = z_i;
%z_q_out = z_q;

z_i_out = fi(z_i, 1, 26, 12);
z_q_out = fi(z_q, 1, 26, 12);
chan2_z_i_out = fi(chan2_z_i, 1, 26, 12);
chan2_z_q_out = fi(chan2_z_q, 1, 26, 12);

%z_i_out = fi(y_i, 1, 26, 12);
%z_q_out = fi(y_q, 1, 26, 12);
%fe      = fi(0, 1, 26, 12);



% Compute Frequency from phase

alpha_slow = cast(1/2^12, 'like', alpha_type); % Coefficient for running average frequency calculation
alpha_fast = cast(1/2^9, 'like', alpha_type);  % Coefficient for running average frequency calculation

if est_enable == 1
    %alpha = 1/2^12; % Coefficient for running average frequency calculation
    alpha = alpha_fast;
    %%    alpha = cast(1/2^8, 'like', alpha_type);  % Coefficient for running average frequency calculation
%    alpha = cast(1/2^9, 'like', alpha_type);  % Coefficient for running average frequency calculation
    %alpha = 1/2^10;  % Coefficient for running average frequency calculation
    %alpha = 1/2^11;  % Coefficient for running average frequency calculation
    %alpha = 1/2^12;  % Coefficient for running average frequency calculation
else
%elseif track_en == 1
    alpha = alpha_slow;
%    alpha = cast(1/2^12, 'like', alpha_type);  % Coefficient for running average frequency calculation
    %alpha = 1/2^12; % Coefficient for running average frequency calculation
    %alpha = 1/2^8;  % Coefficient for running average frequency calculation
    %alpha = 1/2^10;  % Coefficient for running average frequency calculation
    %alpha = 4/2^11;  % Coefficient for running average frequency calculation
%else
%    alpha = cast(0, 'like', alpha_type);
end

%if (est_enable == 1) && ((phiNew < 1 && phiOld < 1) || (phiNew > 0 && phiOld > 0))
if ((est_enable==1) || (track_en==1)) && ((phiNew < 1 && phiOld < 1) || (phiNew > 0 && phiOld > 0))
    %freq_in = cast(phiNew-phiOld, 'like', freq_in);
%    freq_in = (phiNew-phiOld);
    next_freq_in(:) = (phiNew-phiOld);
    next_freq_mean = cast((1-alpha)*freq_mean + alpha*freq_in, 'like', freq_mean);
%    freq_mean = cast((1-alpha)*freq_mean + alpha*freq_in, 'like', freq_mean);
    %freq_mean = cast((1-0)*freq_mean + alpha*freq_in, 'like', freq_mean);
    
%    freq_out = cast(freq_in - freq_dc, 'like', freq_out);
    %freq_out = fi(freq_in - freq_dc, 1,24,12);
    %freq_out = fi(freq_mean, 1,24,16);
    freq_out = freq_mean;
else
    %freq_out = cast(freq_mean*fi(-e,1,8,4), 'like', freq_mean);
    freq_out = freq_mean;
end
%freq_out = fi(0, 1,24,12);
