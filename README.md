This README file contains an overview of the OFDM simulink model 
and instructions on how to run it through the synthesis tools.
================================================================


For information about this repository please see the [wiki page](https://bitbucket.org/gkwoods/wes_capstone/wiki/Home)


This OFDM Modem project is compatible with the following tool versions:

Xilinx ISE 14.7
Matlab R2014a

To run the OFDM modem simulation, launch System Generator 14.7, then use the Matlab file browser to locate and the following file and open.

phy_layer59_delayed_sync.mdl

The Simulink GUI window will launch and within it you will see the OFDM modem model. It consists of six primary components which are described below.

1. tx_upper block:  This block contains the scrambler, convolutional encoder, serializer, qpsk encoder, and the OFDM subcarrier mapping block.

2. tx_ifft_buf_wrap block:  This block contains the IFFT responsible for OFDM modulation, as well as a FIFO to guarantee that the IFFT input is never starved in the event of data bursts. Note that this block is instantiated from a custom library that I built called "My Library."  This library can be accessed using the Simulink Library Browser.  At the heart of tx_ifft_buf_wrap_block lies an IFFT core from the Xilinx Blockset library. To synthesize this block, click down to the System Generator icon, then open it and click "OK" to run synthesis. It is important to note that due to a known bug in the Xilinx System Generator tool, it is necessary to save the model as a .mdl file rather than a .slx file in order to avoid bogus error messages during synthesis of Xilinx blocks.

3. tx_lower block:  This block contains a 1:8 upsampler as well as a digital gain stage that applies a different gain values to different OFDM symbols, depending upon whether they contain the frequency reference sequence, the time reference sequence, or actual data. The digital gain is essentially a work-around to increase the amplitude of the reference signals in order to improve the receiver's synchronization capability. It may be wise to consider adding a window to this stage to reduce spectral emissions caused by the amplified pilot tones, which consist of square waves in the time domain.

4. rx_lower block:  This block contains receiver blocks that reside between the ADC interface and the FFT. This includes time & frequency compensation, phase correction, and timing control. Note that in some places multiple levels of hierarchy are used in order to keep the schematic manageable. This block previously contained DC offset correction sub-block that was based loosely on Matlab code from Toyon lab 4, but it was removed due to inadequate performance and other issues.

5. rx_fft_wrap block:  This block contains the receiver FFT. Like the IFFT, this block is also instantiated from "My Library" and can be synthesized using the procedure described in step (2).

6. rx_upper: This block contains all receive chain blocks between the Rx FFT and the Rx output data port. These include the OFDM symbol demapper, QPSK decoder, de-serializer, viterbi decoder, and descrambler.

7. There is a stream pipeline block placed between the tx_upper and IFFT blocks. The only purpose for this block is to prevent Simulink from failing with Algebraic loop errors. There is actually no Algebraic loop, but Simulink can not determine that because the tx_upper and IFFT blocks are both treated as atomic units as required for HDL synthesis.

Along the lower edge of the Simulink model you will find a set of data checkers that compare Tx and Rx data streams and count the number of mismatches between the two, along with a bit rate. 

The modem design is split into six sections is because Simulink's HDL coder synthesis tool cannot synthesize Xilinx cores, and the Xilinx System Generator synthesis tool cannot synthesize non-Xilinx cores. As a result, the two types of cores must be synthesized separately and then connected within the Xilinx XPS (EDK) tool prior to RTL synthesis. Since the Xilinx IFFT and FFT sit directly in middle of the tx and rx chains respectively, these two blocks effectively split the non-Xilinx part of the model four quadrants, which is the reason for their upper_ and lower_ naming prefexes.

To synthesize non-Xilinx blocks (i.e., all of the above except the IFFT and FFT), right-click on the desired block and select HDL code->HDL Workflow Advisor. In the left-hand tree view, click "Set Target"->"Set Target Device and Synthesis Tool" and select the Target workflow called IP Core Generation. Now, in the left-hand tree view click "HDL Code Generation"->Generate RTL Code and IP Core.  For the "IP core name" entry, set the core name to the name shoen in the Simulink model. For example, tx_upper. Next, right-click on "Generate RTL Code and IP Core". When prompted, set all I/Os to "external port". Now run the remaining synthesis steps by right-clicking "Generate RTL Code and IP Core" again.

When the necessary IP core(s) have been synthesized in Simulink, manually copy the IP core directories from Simulink over to the EDK/pcores directory and launch Xilinx XPS (part of ISE 14.7). Click the Project menu and select "Scan User Repositories" If necessary use XPS to connect any new I/O ports. To generate an FPGA bit file, click "Generate BitStream" in the navigator panel on the left side of the XPS GUI. When synthesis completes the design can be transferred to the SDK by clicking "Export Design."

One other thing to be aware of is the use of HDL model properties in the Simulink model to instantiate pipeline registers during synthesis. These registers are sometimes required in order to meet timing, but they are not reflected in the simulation so this can lead to differences between simulated and synthesized results. One way to deal with this is to use the "generated model" that is produced each time HDL coder is run. The generated model is a copy of the Simulink model that does contain the pipeline registers inserted by HDL coder. It is time consuming to re-generate this model each time a change is made to the model, but it appears to be the best and only way to deal with this particular issue.